<!DOCTYPE html>
<html lang="en" dir="ltr">
<!-- test  -->
  <head>
    <!-- author - Trentino Longhi -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>View Owned Music</title>

    <!-- bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- set stylesheet -->
    <link rel="stylesheet" type="text/css" href="tStyle.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- nav bar style/jq -->
    <link rel="stylesheet" href="navbarstyles.css">
    <script type="text/javascript" src="navbarscript.js"></script>

    <!-- d3 lib/js -->
    <script src="https://d3js.org/d3.v4.js"></script>

  </head>

  <body>
    <!-- new nav bar -->
    <div class="navbar">
      <div class="topnav">
        <a href="#note" class="navbar-left"><img src="note.jpg" height="25"></a>
        <a class="active" href="vhome.php">Home</a>
        <a href="vhome.php#about">About</a>
        <a href="w_feedback.php">Contact</a>
        <a href="vCommissionersDashboard.php">My Dashboard</a>
        <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
      </div>
    </div>

    <!--recordings table-->
    <div class="contentBox">
      <?php
        session_start();
        require_once("db.php");

        //send a query to the database
        $sql =
          "SELECT
            recordingName,
            recordingSubmitDate,
            concat(musicianFirst, ' ', musicianLast) AS musicianName,
            recordingUrl
          FROM
            dbrecording r, dbmusician m
          WHERE
            r.recordingMusicianID = m.MemberID
            AND recordingMusicianID = 8";
        // echo "$sql";
        $result = $mydb->query($sql);

        // table header layout:
        echo "
          <body>
          <table>
          <thead class='outer'><tr>
            <th>Recording Title</th>
            <th>Date Purchased</th>
            <th>Composer Name</th>
            <th>Preview Recording</th>
            <th>Download link</th>
          </tr></thead>
        ";

        // table body layout and loop:
        while($row = mysqli_fetch_array($result)){
          // loop through all the rows in the result array
          echo "
            <tr>
             <td class='outer'>".$row["recordingName"]."</td>
             <td class='inner'>".$row["recordingSubmitDate"]."</td>
             <td class='inner'>".$row["musicianName"]."</td>
             <td class='inner'><audio controls volume='0.7'><source src='".$row["recordingUrl"]."' type='audio/wav'></audio></td>
             <td class='inner'><a href='".$row["recordingUrl"]."' download='".$row["recordingName"]."'><button>link</button></a></td>
            </tr>
          ";
        } // end while loop for table body content
        echo "</table>"
      ?>
    </div> <!-- end table area -->

    <div class="">
      <!-- back button -->
     <br><a href="tViewOwnedMusicAnalytics.php"><button type="button" name="button">view song duration analytics of all submitted recordings</button></a>
    </div>

  </body>

</html>
