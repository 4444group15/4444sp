<!doctype html>
<html>
<head>
  <!-- author - Trentino Longhi -->
  <title>Reception update success!</title>
</head>
<body>
  <?php
    session_start();
    $receptionAccount = "";
    $receptionName = "";
    $err = "";

    if(isset($_GET["ReceptionAccount"])) $receptionAccount=$_GET["ReceptionAccount"];
    if(isset($_GET["ReceptionName"])) $receptionName=$_GET["ReceptionName"];
    // hardcode - these would be (session vars) in actuality
    $memberID = 101;
    $memberType = 'musician';

    // add a new payment method
    require_once("db.php");
    $sql =
      "SELECT
        ReceptionAccount,
        ReceptionName
      FROM
        dbreceptionmethod
      WHERE
        ReceptionAccount = '$receptionAccount'";
    // echo "sql: $sql<br>";
    // echo "rname: $receptionName<br>";

    //format
    echo "
    <style>
      table, th, td {
        border:1px solid black;
      }
    </style>
    ";
    // table header layout:
    echo "
      <form method='get' action='tManageReceptionUpdate2.php?'>
        <input type='hidden' name='ReceptionAccount' value='$receptionAccount'></input><br>
        Reception Method Name: <input type='text' name='ReceptionName' value='$receptionName'></input><br>
      <input type='submit' name='submit' value='Submit'></input>
    </form>
    ";
   ?>

   <!-- back button -->
  <br><br><br><a href="tManageReception.php"><button type="button" name="button">return to manage receptions page</button></a>

</body>
</html>
