<!doctype html>
<html>
<head>
  <!-- author - Trentino Longhi -->
  <title>Reception addition success!</title>
</head>
<body>
  <?php
    session_start();
    if(isset($_POST["ReceptionAccount"])) $receptionAccount=$_POST["ReceptionAccount"];
    if(isset($_POST["ReceptionRouting"])) $receptionRouting=$_POST["ReceptionRouting"];
    if(isset($_POST["ReceptionFirstName"])) $receptionFirstName=$_POST["ReceptionFirstName"];
    if(isset($_POST["ReceptionLastName"])) $receptionLastName=$_POST["ReceptionLastName"];
    if(isset($_POST["ReceptionName"])) $receptionName=$_POST["ReceptionName"];
    // hardcode - these would be (cookies?)
    $memberID = 101;
    $memberType = 'musician';
  ?>

  <!-- display retrieved form data -->
  <p>
    <strong><?php echo $receptionAccount; ?></strong> is a bank account with routing num
    <strong><?php echo $receptionRouting; ?></strong>, account name
    <strong><?php echo "$receptionFirstName $receptionLastName"; ?></strong>, and Reception Name
    <strong><?php echo $receptionName; ?></strong>.
    <br>
  </p>

  <?php
    // add a new payment method
    require_once("db.php");
    $sql =
      "INSERT INTO
        dbreceptionmethod(MemberID, MemberType, ReceptionAccount, ReceptionRouting, ReceptionFirstName, ReceptionLastName, ReceptionName)
      VALUES
        ('$memberID', '$memberType', '$receptionAccount', '$receptionRouting', '$receptionFirstName', '$receptionLastName', '$receptionName')";
    $result=$mydb->query($sql);

    if ($result==1) { // if db table was modified
      echo "result: success! added a reception method for $receptionName.<br><br>";
    } else { // if db table fialed to mod
      echo "result: failure. failed to add a reception method for $receptionName.<br><br>";
    } // end result check
    // echo "$sql <br><br>"; // helps debugging

    // table time!

    //format
    echo "
    <style>
      table, th, td {
        border:1px solid black;
      }
    </style>
    ";

    // table header layout:
    echo "
      <table>
        <thead><tr>
        <th>Card Number</th>
        <th>CVC</th>
        <th>Cardholder Name</th>
        <th>Payment Method Name</th>
        </tr></thead>
    ";

    // table body
    echo "
        <tr>
         <td>$receptionAccount</td>
         <td>$receptionRouting</td>
         <td>$receptionFirstName $receptionLastName</td>
         <td>$receptionName</td>
        </tr>
      </table>
    ";
   ?>

   <!-- back button -->
  <br><br><br><a href="tManageReception.php"><button type="button" name="button">return to manage reception page</button></a>

</body>
</html>
