
<?php
//View the sheet music uploaded by composers
//need session

$Request_Search= "";
$Sold_or_Not="";
?>
 <!DOCTYPE html>
 <html lang="" dir="ltr">
   <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>View My Sheet Music</title>

     <!-- bootstrap -->
     <link href="css/bootstrap.min.css" rel="stylesheet" />
     <script src="jquery-3.1.1.min.js"></script>
     <script src="js/bootstrap.min.js"></script>

     <!-- set stylesheet -->
     <meta name="viewport" content="width=device-width, initial-scale=1">

     <!-- nav bar style/jq -->
     <link rel="stylesheet" href="navbarstyles.css">
     <script type="text/javascript" src="navbarscript.js"></script>

   </head>
   <style>
      h1{
        text-shadow: 3px 2px white;
      }
      body {
      background-color: #F0FFFF;
      }
      button{
        background-color: white;
        color: black;
      }
      table.blueTable {
        border: 1px solid ;
        background-color: #FFE4C4;
        width: 80%;
        text-align: center;
        border-collapse: collapse;
        /* hello */

      }
      table.blueTable td, table.blueTable th {
        border: 1px solid;
        padding: 3px 2px;
      }

      .inner{
      background-color: #FFFACD
      }
    </style>
   <body>
     
     <div class="navbar">
       <div class="topnav">
         <a href="#note" class="navbar-left"><img src="note.jpg" height="25"></a>
         <a class="active" href="vhome.php">Home</a>
         <a href="vhome.php#about">About</a>
         <a href="w_feedback.php">Contact</a>
         <a href="vComposerDashboard.php">My Dashboard</a>
         <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
       </div>
     </div>

    <div align= center>
     <h1>View Sheet Music Sold by Composer</h1>
    </div>

     <div align= center>

     <table class= "blueTable">
         <tr>
         <td><strong>
           Song Name:
         </strong></td>
         <td><strong>
          Instruments List:
         </strong></td>
         <td><strong>
           Download Count:
         </strong></td>
         <td><strong>
           Price:
         </strong></td>
         <td><strong>
           Recordings Count:
         </strong></td>
         <td><strong>
           Edit:
         </strong></td>
         <td><strong>
           Delete:
         </strong></td>
         <td><strong>
           Add comment:
         </strong></td>

       </tr>
       <tr>
         <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>"/>


           <label>Request Search:</label>
           <input name="Request_Search" type="text" value="<?php echo $Request_Search; ?>"/>
           <select name="soldYN">
             <option value="all" selected></option>
             <option value="sold">sold</option>
             <option value="not sold">not sold</option>
           </select>


           <input type="submit" name="Search" value="Search">
           <br />
        </form>

          <?php
          require_once("db.php");
          if(isset($_POST['soldYN'])) {$Sold_or_Not=$_POST['soldYN'];}
          else {$Sold_or_Not='all';}

          echo "$Sold_or_Not";

           //if (isset($_POST["number"])) {
           if(isset($_POST["Request_Search"])) {$Request_Search=$_POST["Request_Search"];}
           else {$Request_Search='';}


           if($Sold_or_Not=='all'){
            $sql = "select * from dbsheetmusic where
            songname like '%$Request_Search%'
            OR songinstrumentslist like '%$Request_Search%'
            OR songdownloadscount like '%$Request_Search%'
            OR songprice like '%$Request_Search%'
            OR songrecordingscount like '%$Request_Search%'
            ";}

           elseif($Sold_or_Not=='not sold'){
           $sql = "select * from dbsheetmusic where (soldYN=0) AND
           (songname like '%$Request_Search%'
           OR songinstrumentslist like '%$Request_Search%'
           OR songdownloadscount like '%$Request_Search%'
           OR songprice like '%$Request_Search%'
           OR songrecordingscount like '%$Request_Search%')
           ";}

           elseif($Sold_or_Not=='sold'){
           $sql = "select * from dbsheetmusic where (soldYN=1) AND
           (songname like '%$Request_Search%'
           OR songinstrumentslist like '%$Request_Search%'
           OR songdownloadscount like '%$Request_Search%'
           OR songprice like '%$Request_Search%'
           OR songrecordingscount like '%$Request_Search%')
           ";}


           if($Request_Search==""){
             if($Sold_or_Not=='all'){
              $sql = "select * from dbsheetmusic";}

             elseif($Sold_or_Not=='not sold'){
             $sql = "select * from dbsheetmusic where (soldYN=0)";}

             elseif($Sold_or_Not=='sold'){
             $sql = "select * from dbsheetmusic where (soldYN=1)";}}

           $result = $mydb->query($sql);

           while ($row = mysqli_fetch_array($result)) {
             echo"
            <tr>
             <td class='outer'>".$row["songName"]."</td>
             <td class='inner'>".$row["songInstrumentsList"]."</td>
             <td class='inner'>".$row["songDownloadsCount"]."</td>
             <td class='inner'>".$row["songPrice"]."</td>
             <td class='inner'>".$row["songRecordingsCount"]."</td>
             <td class='inner'><a href='jInformationForm.php?SheetMusicID=".$row['SheetMusicID']."&songName=".$row["songName"]."&songInstrumentsList=".$row["songInstrumentsList"]."&songPrice=".$row["songPrice"]."'><button>Edit</button></a></td>
             <td class='inner'><a href='jDeleteMusic.php?SheetMusicID=".$row['SheetMusicID']."&songName=".$row["songName"]."'><button>Delete</button></a></td>
             <td class='inner'><a href='jAddComment.php?SheetMusicID=".$row['SheetMusicID']."&songName=".$row["songName"]."'><button>Add Comment</button></a></td>
            </tr>
            ";
           }

            echo "</table>"

             ?>

          </div>
          <div align= center>
          <br><br><a href="jGraph.php"><button type="button" class="btn btn-primary" name="button">Show counts for comments</button></a>
          </div>
  </body>
 </html>
