<?php
//this page allows composers to view the requests which are available for submissions
//requests written by commissioners

//in sql statement, looks for requestStatus=1
//1 means active, open for submissions



$Request_Search= "";
$Active_or_Not="";

?>
 <!DOCTYPE html>
 <html lang="" dir="ltr">
   <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>View Requests (Composer View)</title>

     <!-- bootstrap -->
     <link href="css/bootstrap.min.css" rel="stylesheet" />
     <script src="jquery-3.1.1.min.js"></script>
     <script src="js/bootstrap.min.js"></script>

     <!-- set stylesheet -->
     <link rel="stylesheet" type="text/css" href="tStyles.css">
     <meta name="viewport" content="width=device-width, initial-scale=1">

     <!-- nav bar style/jq -->
     <link rel="stylesheet" href="navbarstyles.css">
     <script type="text/javascript" src="navbarscript.js"></script>
     <style>.errlabel {color:red;}</style>
     <style media="screen">
     body{
       background-color: lightgray;
     }
    th{
         background-color: steelblue;
         color:white;
    }
    .move{
         margin-left: 20px;
    }
     </style>

   </head>
   <body>
     <div class="navbar">
       <div class="topnav">
         <a class="navbar-left"><img src="note.jpg" height="25"></a>
         <a class="active" href="vhome.php">Home</a>
         <a href="vhome.php#about">About</a>
         <a href="w_feedback.php">Contact</a>
         <a href="vComposerDashboard.php">My Dashboard</a>
         <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
       </div>
     </div>

     <h1 style="margin-left: 20px;">View Requests Available</h1>
     <table border="1px" style="margin-left: 20px;">
         <tr>
         <th>
           Request ID:
         </th>
         <th>
           Commissioner ID:
         </th>
         <th>
           Request Content:
         </th>
         <th>
           Request Submit Date:
         </th>
         <th>
           Status:
         </th>
       </tr>
       <tr>
         <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">


           <label style="margin-left: 20px;">Request Search:</label>
           <input name="Request_Search" type="text" value="<?php echo $Request_Search; ?>"/>
           <label style="margin-left: 20px;">Request Status:</label>
           <!-- <select name="activeYN">
             <option value="all" selected></option>
             <option value="active">Active</option>
             <option value="inactive">Inactive</option>
           </select> -->
           <input type="submit" name="Search" value="Search">
           <br />
        </form>

          <?php
          require_once("db.php");
          //if(isset($_POST['activeYN'])) {$Active_or_Not=$_POST['activeYN'];}
          //else {$Active_or_Not='all';}

           //if (isset($_POST["number"])) {
           if(isset($_POST["Request_Search"])) {$Request_Search=$_POST["Request_Search"];}
           else {$Request_Search='';}
           //$Request_Search="number";

           if(!empty($Request_Search)){
             $sql = "select * from dbrequest where
             requestid like '%$Request_Search%'
             OR commissionerid like '%$Request_Search%'
             OR requestcontent like '%$Request_Search%'
             OR requestsubmitdate like '%$Request_Search%'
             AND (requestActive=1)
             ";}


           // elseif($Active_or_Not=='active'){
           //   $sql = "select * from dbrequest where (requestid like '%$Request_Search%'
           //   OR commissionerid like '%$Request_Search%'
           //   OR requestcontent like '%$Request_Search%'
           //   OR requestsubmitdate like '%$Request_Search%')
           //   AND (requestActive=1)
           //   ";}
           //
           // elseif($Active_or_Not=='inactive'){
           //   $sql = "select * from dbrequest where (requestid like '%$Request_Search%'
           //   OR commissionerid like '%$Request_Search%'
           //   OR requestcontent like '%$Request_Search%'
           //   OR requestsubmitdate like '%$Request_Search%')
           //   AND (requestActive=0)
           //   ";}

           if($Request_Search==""){
              $sql = "select * from dbrequest where (requestActive=1)";}

             // elseif($Active_or_Not=='active'){
             // $sql = "select * from dbrequest where (requestActive=1)";}
             //
             // elseif($Active_or_Not=='inactive'){
             // $sql = "select * from dbrequest where (requestActive=0)";}}

           $result = $mydb->query($sql);

           while ($row = mysqli_fetch_array($result)) {
             echo "
               <tr>
                <td class='recordingID'>".$row["requestID"]."</td>
                <td class='rName'>".$row["commissionerID"]."</td>
                <td class='rAlbum'><a href=vuploadsubmission.php>".$row["requestContent"]."</a></td>
                <td class='rMusicianID'>".$row["requestSubmitDate"]."</td>
                <td class='rSubmitDate'>".$row["requestActive"]."</td>
               </tr>
             ";
           } // end while loop for table body content
           echo "</table>"


           // $oneArray = Array();
           // $twoArray = Array();
           // $threeArray = Array();
           // $fourArray = Array();
           // $fiveArray = Array();
           // while ($row = mysqli_fetch_array($result)) {
           //    $oneArray[] =  $row['requestID'];
              // $twoArray[] = $row['commissionerID'];
              // $threeArray[] = $row['requestContent'];
              // $fourArray[] = $row['requestSubmitDate'];
              // $fiveArray[] = $row['requestActive'];}
          // echo "<td>".implode("</br>", $oneArray)."</td>";
          // echo "<td>".implode("</br>", $twoArray)."</td>";
          // echo "<td>".implode("</br>", $threeArray)."</td>";
          // echo "<td>".implode("</br>", $fourArray)."</td>";
          // echo "<td>".implode("</br>", $fiveArray)."</td>";
            ?>
       </tr>

   </body>
 </html>
