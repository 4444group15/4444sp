<?php

session_start();
  //need to get the ID from the username table
  //then go to the Musician, Composer, or Commissioner table
  //and get the ID from there
//This page allows users to give feedback or ask for help

  $feedbackID = "";
  $userID = $_SESSION["userid"];
  $type=$_SESSION['acctype'];
  $feedbackContent = "";
  //$reqActive = 1;
  $feedbackSubmitDate = date("Y-m-d H:i:s");
  $err = false;

  if (isset($_POST["submit"])) {
      //if(isset($_POST["requestID"])) $reqID=$_POST["requestID"];
      //if(isset($_POST["commissionerID"])) $userID=$_POST["commissionerID"];
      if(isset($_POST["feedbackContent"])) $feedbackContent=$_POST["feedbackContent"];
      //if(isset($_POST["requestSubmitDate"])) $reqSubmitDate=$_POST["requestSubmitDate"];
      //if(isset($_POST["requestActive"])) $reqActive=$_POST["requestActive"];

      require_once("db.php");
      $feedbackID = "select Max(feedbackID) from dbfeedback order by feedbackID desc";
      $result=$mydb->query($feedbackID);
      $row = mysqli_fetch_array($result);
      $newFeedbackID=$row[0]+1;
      //echo $newFeedbackID;


      $sql = "insert into dbfeedback values ('$newFeedbackID','$userID','$feedbackContent','$feedbackSubmitDate',0)";
      //echo $sql;

      $result=$mydb->query($sql);

      if ($result==1) {
        echo "<script>alert('Feedback received. Thank you!');</script>";
        //echo "<p>A new record has been created</p>";
      }
      else {
        $err = true;
      }

      // if(!empty($feedbackID) && !empty($userID) && !empty($feedbackSubmitDate) && !empty($feedbackContent)) {
      //   echo "<script>alert('Feedback received. Thank you!');</script>";
      //   header("HTTP/1.1 307 Temprary Redirect"); //
      //   header("Location: w_view_requests_commissioner_view.php");
      // } else {
      //   $err = true;
      // }


  }
 ?>

<!doctype html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Provide Feedback</title>

      <!-- bootstrap -->
      <link href="css/bootstrap.min.css" rel="stylesheet" />
      <script src="jquery-3.1.1.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <!-- set stylesheet -->
      <link rel="stylesheet" type="text/css" href="tStyles.css">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- nav bar style/jq -->
      <link rel="stylesheet" href="navbarstyles.css">
      <script type="text/javascript" src="navbarscript.js"></script>
      <style>.errlabel {color:red;}</style>
      <style media="screen">
        body{
          background-color:lightgray;
        }
      </style>
      <style>
        #animation {
          position: relative;
          -webkit-animation: run linear 4s 0.2s 1 normal;
          animation: run linear 4s 0.2s 1 normal;
          /* z-index: 2; */
        }

        @keyframes run{
        0% { left: 620px;}
        50%{ left : 50;}
        100%{ left: 10;}}

        @-webkit-keyframes run {
        0% { left: 620px;}
        50%{ left : 50;}
        100%{ left: 10;}}
        /* @keyframes spin {
          0% {opactiy: 1; left: 0px; top: 0px; }
          25% {opacity: 1; left: 0px; top: 0px; }
          50% {opacity: ; left: 0px; top: 0px;}
          75% {opacity: 1; left: 0px; top: 0px;}
          100% {opacity: 1; left: 0px; top: 0px; } */


          /* 0% {opactiy: 1; left: 0px; top: 0px; }
          10% {opacity: 1; left: 50px; top: 50px; }
          20% {opacity: ; left: 100px; top: -50px;}
          30% {opacity: 1; left: 150px; top: 100px;}
          40% {opacity: 1; left: 200px; top: -50px; }
          50% {opacity: 1; left: 250px; top: 100px; }
          60% {opacity: 1; left: 300px; top: -50px; }
          70% {opacity: 1; left: 350px; top: 150px; }
          80% {opacity: 1; left: 400px; top: -50px; }
          90% {opacity: 1; left: 450px; top: 150px; }
          100% {opacity: 1; left: 500px; top: -50px; } */
        }

         @-webkit-keyframes spin {
           0% {opactiy: 1; left: 0px; top: 0px; }
           25% {opacity: 1; left: 0px; top: 0px; }
           50% {opacity: ; left: 0px; top: 0px;}
           75% {opacity: 1; left: 0px; top: 0px;}
           100% {opacity: 1; left: 0px; top: 0px; }

           /*
          0% {opactiy: 1; left: 0px; top: 0px; }
          10% {opacity: 1; left: 50px; top: 50px; }
          20% {opacity: ; left: 100px; top: -50px;}
          30% {opacity: 1; left: 150px; top: 100px;}
          40% {opacity: 1; left: 200px; top: -50px; }
          50% {opacity: 1; left: 250px; top: 100px; }
          60% {opacity: 1; left: 300px; top: -50px; }
          70% {opacity: 1; left: 350px; top: 150px; }
          80% {opacity: 1; left: 400px; top: -50px; }
          90% {opacity: 1; left: 450px; top: 150px; }
          100% {opacity: 1; left: 500px; top: -50px; } */
        }
        .image{
          z-index: 1;
        }
      </style>

    </head>


<body>
  <div class="navbar">
    <div class="topnav">
      <a class="navbar-left"><img src="note.jpg" height="25"></a>
      <a class="active" href="vhome.php">Home</a>
      <a href="vhome.php#about">About</a>
      <a href="w_feedback.php">Contact</a>
      <?php
      $goto='';
      if($type="Musician") $goto='vMusicDashboard.php';
      if($type="Composer") $goto='vComposerDashboard.php';
      if($type="Commissioner") $goto='vCommissionersDashboard.php'; ?>
      <a href="<?php echo $goto; ?>">My Dashboard</a>
      <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
    </div>
  </div>

  <div style="z-index:10;" class="floatingImg">

  <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">


    <!-- <label>Request ID:</label>
    <input name="requestID" type="number" value="<?php //echo $reqID; ?>"/>
    <?php
    //  if ($err && $reqID=="") {
      //  echo "<label class='errlabel'>Error: Please enter a commissioner name</label>";
    //  }
    ?>
  </br> -->
  <h1 style="margin-left:20px">How are we doing?</h1>
  <p style="margin-left:20px">Please let us know how we can improve your experience on our platform. If there is any way we can assist you or if you are having technical difficulties, please let us know.</p>





    <label style="margin-left:20px">Feedback Form:</label>
    <br/>
    <div style="margin-left:20px">
      <!-- <input style="width: 300px; height: 200px; vertical-align: top; padding: 10px 0 200px 0;" name="requestContent" type="textarea" value="<?php echo $reqContent; ?>"/> -->
      <textarea  name="feedbackContent" rows="8" cols="80" value="<?php echo $feedbackContent; ?>"></textarea>
      <?php
        if ($err && empty($reqContent)) {
          echo "<label class='errlabel'>Error: Please enter a request description</label>";
        }
      ?>
    </div>
    <br />



    <!-- <label>Request Submit Date:</label>
    <input type="text" name="requestSubmitDate" value="<?php //echo $reqSubmitDate; ?>"/>
    <?php
    //  if ($err && empty($reqSubmitDate)) {
      //  echo "<label class='errlabel'>Error: Please enter a valid unit price</label>";
      //}
    ?>
    <br /> -->



    <!-- <label>Request Active:</label>
    <input type="text" name="requestActive" value="<?php //echo $reqActive; ?>"/>
    <?php
  //    if ($err && empty($reqActive)) {
    //    echo "<label class='errlabel'>Error: Please enter a 1</label>";
    //  }
    ?>
    <br /> -->

    <input style="margin-left:20px" type="submit" name="submit" value="Submit" />
    <br />

  </form>

<?php


      // require_once("db.php");
      // $sql = "insert into dbrequest values ('$reqID','$userID','$reqContent','$reqSubmitDate','$reqActive')";
      // echo $sql;
      //
      // $result=$mydb->query($sql);
      //
      // if ($result==1) {
      //   echo "<p>A new record has been created</p>";
      // }


?>
</div>
<div id="image" class="floatingImg">
  <img STYLE="position:absolute; TOP:250px; LEFT:850px;" id="animation" src="./images/musicLine.png" alt="" height="100px" width="300px">
</div>

</body>
</html>
