<?php
session_start();
//this page allows composers to view the requests which are available for submissions
//requests written by commissioners

//in sql statement, looks for requestStatus=1
//1 means active, open for submissions



$Request_Search= "";
$Active_or_Not="";
$myRequests="";
$myRequests=$_SESSION["userid"];


?>
 <!DOCTYPE html>
 <html lang="" dir="ltr">
   <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>View Requests (Commissioner View)</title>

     <!-- bootstrap -->
     <link href="css/bootstrap.min.css" rel="stylesheet" />
     <script src="jquery-3.1.1.min.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script type="text/javascript">

     function deleteButton(a) {
          // event.target will be the input element.
          var td = event.target.parentNode;
          var tr = td.parentNode; // the row to be removed
          var id=a.name;
          console.log(id);

          $.ajax({
            url:'w_delete_requests_commissioner.php?id='+id,
            async:true,
            success: function(result){
               $("#contentArea").html(result);

           }
         });
          //ajax
          //deleteMessageByid.php+id
          tr.parentNode.removeChild(tr);
     }

     function updateButton(a) {
          // event.target will be the input element.
          //var td = event.target.parentNode;

          //var tr = td.parentNode; // the row to be removed
          var id=a.name;
          console.log(id);

          $.ajax({
            url:'w_update_requests_commissioner.php?id='+id,
            async:true,
            success: function(result){
              window.location.href = "w_update_requests_commissioner.php";
               //$("#contentArea").html(result);
           }
         });
          //ajax
          //deleteMessageByid.php+id
          //tr.parentNode.removeChild(tr);

     }

     </script>

     <!-- set stylesheet -->
     <link rel="stylesheet" type="text/css" href="tStyles.css">
     <meta name="viewport" content="width=device-width, initial-scale=1">

     <!-- nav bar style/jq -->
     <link rel="stylesheet" href="navbarstyles.css">
     <script type="text/javascript" src="navbarscript.js"></script>
     <style>.errlabel {color:red;}</style>
     <style media="screen">
     body{
       background-color: lightgray;
     }
       th{
         background-color: steelblue;
         color:white;
       }

      .move{
        margin-left: 20px;
      }
     </style>

   </head>
   <body>
     <div class="navbar">
       <div class="topnav">
         <a class="navbar-left"><img src="note.jpg" height="25"></a>
         <a class="active" href="vhome.php">Home</a>
         <a href="vhome.php#about">About</a>
         <a href="w_feedback.php">Contact</a>
         <a href="vCommissionersDashboard.php">My Dashboard</a>
         <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
       </div>
     </div>

     <h1 class="move">View Requests</h1>
     <table class="move" border="1px">
         <tr>
         <th>
           Request ID:
         </th>
         <th>
           Commissioner ID:
         </th>
         <th>
           Request Content:
         </th>
         <th>
           Request Submit Date:
         </th>
         <!-- <th>
           Status:
         </th> -->
         <?php if(isset($_POST['myRequests'])) {
           echo "<th>Delete:</th><th>Update:</th>";
         }
         ?>

       </tr>
       <tr>
         <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
           <br/>
           <input  class="move" type="submit" name="myRequests" value="View My Requests">
           <br/>
           <br/>


           <label class="move" >Request Search:</label>
           <input name="Request_Search" type="text" value="<?php echo $Request_Search; ?>"/>
           <label class="move">Request Status:</label>
           <select name="activeYN">
             <option value="all" selected></option>
             <option value="active">Active</option>
             <option value="inactive">Inactive</option>
           </select>
           <input type="submit" name="Search" value="Search">

           <br />
        </form>

          <?php
          require_once("db.php");

          if(!isset($_POST['myRequests'])) {


          if(isset($_POST['activeYN'])) {$Active_or_Not=$_POST['activeYN'];}
          else {$Active_or_Not='all';}


           //if (isset($_POST["number"])) {
           if(isset($_POST["Request_Search"])) {$Request_Search=$_POST["Request_Search"];}
           else {$Request_Search='';}
           //$Request_Search="number";

           if($Active_or_Not=='all'){
             $sql = "select * from dbrequest where
             requestid like '%$Request_Search%'
             OR commissionerid like '%$Request_Search%'
             OR requestcontent like '%$Request_Search%'
             OR requestsubmitdate like '%$Request_Search%'
             ORDER BY requestid ASC
            ";}


           elseif($Active_or_Not=='active'){
             $sql = "select * from dbrequest where (requestid like '%$Request_Search%'
             OR commissionerid like '%$Request_Search%'
             OR requestcontent like '%$Request_Search%'
             OR requestsubmitdate like '%$Request_Search%')
             AND (requestActive=1)
             ORDER BY requestid ASC
             ";}

           elseif($Active_or_Not=='inactive'){
             $sql = "select * from dbrequest where (requestid like '%$Request_Search%'
             OR commissionerid like '%$Request_Search%'
             OR requestcontent like '%$Request_Search%'
             OR requestsubmitdate like '%$Request_Search%')
             AND (requestActive=0)
             ORDER BY requestid ASC

             ";}

           if($Request_Search==""){
             if($Active_or_Not=='all'){
              $sql = "select * from dbrequest ORDER BY requestid ASC";}

             elseif($Active_or_Not=='active'){
             $sql = "select * from dbrequest where (requestActive=1) ORDER BY requestid ASC";}

             elseif($Active_or_Not=='inactive'){
             $sql = "select * from dbrequest where (requestActive=0) ORDER BY requestid ASC";}}

             $result = $mydb->query($sql);

             while ($row = mysqli_fetch_array($result)) {
               $reqID=$row["requestID"];
               echo "
                 <tr>
                  <td class='recordingID'>".$row["requestID"]."</a></td>
                  <td class='rName'>".$row["commissionerID"]."</td>
                  <td class='rAlbum'>".$row["requestContent"]."</td>
                  <td class='rMusicianID'>".$row["requestSubmitDate"]."</td>

                 </tr>
               ";
               //<td class='rSubmitDate'>".$row["requestActive"]."</td>
             } // end while loop for table body content
             echo "</table>";}

           if(isset($_POST['myRequests'])) {
                 $sql = "select * from dbrequest where
                 commissionerid like '$myRequests'
                 ORDER BY requestid ASC";

                 $result = $mydb->query($sql);

                 while ($row = mysqli_fetch_array($result)) {
                   echo "
                     <tr>
                      <td class='requestID'>".$row["requestID"]."</a></td>
                      <td class='comID'>".$row["commissionerID"]."</td>
                      <td class='songName'><a href='tViewSubmissions.php'>".$row["requestContent"]."</td>
                      <td class='rMusicianID'>".$row["requestSubmitDate"]."</td>

                      <td><input type='submit' onclick='deleteButton(this)' name='".$row["requestID"]."' value='delete'></button></td>
                      <td><input type='submit' onclick='updateButton(this)' name='".$row["requestID"]."' value='update'></button></td>


                     </tr>
                   ";
                   //<td class='rSubmitDate'>".$row["requestActive"]."</td>
                 } // end while loop for table body content
                 echo "</table>";}

              // if(isset($_POST["deleteButton"])){
              //
              //   $sql = "select * from dbrequest where
              //   requestid like '$reqID'";
              //
              //   $result = $mydb->query($sql);
              //
              // }








          //  $oneArray = Array();
          //  $twoArray = Array();
          //  $threeArray = Array();
          //  $fourArray = Array();
          //  $fiveArray = Array();
          //  while ($row = mysqli_fetch_array($result)) {
          //     $oneArray[] =  $row['requestID'];
          //     $twoArray[] = $row['commissionerID'];
          //     $threeArray[] = $row['requestContent'];
          //     $fourArray[] = $row['requestSubmitDate'];
          //     $fiveArray[] = $row['requestActive'];}
          // echo "<td>".implode("</br>", $oneArray)."</td>";
          // echo "<td>".implode("</br>", $twoArray)."</td>";
          // echo "<td>".implode("</br>", $threeArray)."</td>";
          // echo "<td>".implode("</br>", $fourArray)."</td>";
          // echo "<td>".implode("</br>", $fiveArray)."</td>";
            ?>
       </tr>

       <div id="contentArea">&nbsp;</div>

   </body>
 </html>
