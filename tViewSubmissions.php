<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">

  <head>
    <!-- author - Trentino Longhi -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>View Submissions</title>

    <!-- bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- set stylesheet -->
    <link rel="stylesheet" type="text/css" href="tStyle.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- nav bar style/jq -->
    <link rel="stylesheet" href="navbarstyles.css">
    <script type="text/javascript" src="navbarscript.js"></script>

  </head>

  <body>
    <!-- new nav bar -->
    <div class="navbar">
      <div class="topnav">
        <a href="#note" class="navbar-left"><img src="note.jpg" height="25"></a>
        <a class="active" href="vhome.php">Home</a>
        <a href="vhome.php#about">About</a>
        <a href="w_feedback.php">Contact</a>
        <a href="vComposerDashboard.php">My Dashboard</a>
        <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
      </div>
    </div>

    <!-- result table -->
    <div class="contentBox">
      <?php
        require_once("db.php");

        // temp harcode - would be a (session)
        $memberID = 3;

        //send a query to the database
        $sql =
          "SELECT
            requestID,
            concat(commissionerFirstName, ' ', commissionerLastName) AS commissionerName,
            requestContent
          FROM
            dbrequest rq, dbcommissioner c
          WHERE
            rq.commissionerID = c.MemberID
            AND MemberID = $memberID";
        // echo $sql;
        $result = $mydb->query($sql);

        // table header layout:
        echo "
          <body>
          <table>
          <thead class='outer'><tr>
            <th>Request ID</th>
            <th>Name</th>
            <th>Details</th>
          </tr></thead>
        ";

        // table body layout and loop:
        while($row = mysqli_fetch_array($result)){
          // loop through all the rows in the result array
          echo "
            <tr>
              <td class='outer'>".$row["requestID"]."</td>
              <td class='inner'>".$row["commissionerName"]."</td>
              <td class='inner'>".$row["requestContent"]."</td>
            </tr>
          ";
        } // end while loop for table body content
        echo "</table>";
      ?>

    </div> <!-- end table area -->

  </body>

</html>
