<?php
  session_start();
  // author: trentino longhi
  // define local vars to store form data entered by user

  $receptionAccount = "";
  $receptionRouting = "";
  $receptionFirstName = "";
  $receptionLastName = "";
  $receptionName = "";
  $err = "";

  if (isset($_POST["submit"])) { // checks whether submit was clicked
    if(isset($_POST["ReceptionAccount"])) $receptionAccount=$_POST["ReceptionAccount"];
    if(isset($_POST["ReceptionRouting"])) $receptionRouting=$_POST["ReceptionRouting"];
    if(isset($_POST["ReceptionFirstName"])) $receptionFirstName=$_POST["ReceptionFirstName"];
    if(isset($_POST["ReceptionLastName"])) $receptionLastName=$_POST["ReceptionLastName"];
    if(isset($_POST["ReceptionName"])) $receptionName=$_POST["ReceptionName"];
    // return true if an empty string is returned

    if(// if everything is filled out
      !empty($receptionAccount) &&
      !empty($receptionRouting) && ($receptionRouting < 1000000000) &&
      !empty($receptionFirstName) &&
      !empty($receptionLastName) &&
      !empty($receptionName)) {
        header("HTTP/1.1 307 Temprary Redirect"); // needed for temp rediredirects, makes sure the second redirect will work
        header("Location: tManageReceptionSuccess.php"); // redirect to the success page
      } else { // if something goes wrong
        echo "PROBLEM";
        $err = true; // change err flag to true
        // it will automatically display error msgs we embedded throughout the doc
    } // end if form is filled out
  } // end if submit btn clicked
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

  <head>
    <!-- author - Trentino Longhi -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manage Reception Methods</title>

    <!-- bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- set stylesheet -->
    <link rel="stylesheet" type="text/css" href="tStyle.css">

    <!-- nav bar style/jq -->
    <link rel="stylesheet" href="navbarstyles.css">
    <script type="text/javascript" src="navbarscript.js"></script>

  </head>

  <body>

    <!-- new nav bar -->
    <div class="navbar">
      <div class="topnav">
        <a href="#note" class="navbar-left"><img src="note.jpg" height="25"></a>
        <a class="active" href="vhome.php">Home</a>
        <a href="vhome.php#about">About</a>
        <a href="w_feedback.php">Contact</a>
        <a href="vComposerDashboard.php">My Dashboard</a>
        <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
      </div>
    </div>

    <!--table of all reception methods-->
    <div class="contentBox">
      <h2>Reception Method List</h2>
      <?php
        require_once("db.php");

        //send a query to the database
        $sql =
          "SELECT
            ReceptionAccount,
            concat(ReceptionFirstName, ' ', ReceptionLastName) AS ReceptionUsername,
            ReceptionName
          FROM
            dbreceptionmethod";
        // echo "$sql";
        $result = $mydb->query($sql);
        //$result should be a resultset

        // table header layout:
        echo "
          <body>
          <table>
          <thead class='outer'><tr>
            <th>Account Number</th>
            <th>Name on Account</th>
            <th>Reception Method Name</th>
            <th>Update?</th>
            <th>Delete?</th>
          </tr></thead>
        ";

        // table body layout and loop:
        while($row = mysqli_fetch_array($result)){
          // loop through all the rows in the result array
          echo "
            <tr>
              <td class='outer'>".$row["ReceptionAccount"]."</td>
              <td class='inner'>".$row["ReceptionUsername"]."</td>
              <td class='inner'>".$row["ReceptionName"]."</td>
              <td class='inner'><a href='tManageReceptionUpdate1.php?ReceptionAccount="
                .$row['ReceptionAccount']."&ReceptionName="
                .$row['ReceptionName']."'><button>update</button></a></td>
              <td class='inner'><a href='tManageReceptionDelete.php?ReceptionAccount="
                .$row['ReceptionAccount']."&ReceptionUsername="
                .$row['ReceptionUsername']."'><button>delete</button></a></td>
            </tr>
          ";
        } // end while loop for table body content

        echo "</table>";

        //format
        echo "
          <style>
            table, th, td {
              border:1px solid black;
              padding:3px;
            }
          </style>
        ";
      ?> <!-- end php -->
    </div> <!-- end table area -->

    <!-- add reception method form -->
    <div class="contentBox">
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
          <h2>Add new reception method</h2>
          <table class="addNew">
            <tr>
              <!-- card num and cvc -->
              <td>Account Number: </td>
              <td><input required type="tel" name="ReceptionAccount" size="20"  value="<?php echo $receptionAccount; ?>"  maxlength="10"></td>
              <td>Routing Number: </td>
              <td><input required type="tel" name="ReceptionRouting" size="9" value="<?php echo $receptionRouting; ?>" maxlength="9"></td>
            </tr>
            <tr>
              <td>Reception method name: </td>
              <td><input required type="text" name="ReceptionName" size="20" value="<?php echo $receptionName; ?>"></td>
            </tr>
            <tr>
              <td>Reception First Name: </td>
              <td><input required type="text" name="ReceptionFirstName" size="20"  value="<?php echo $receptionFirstName; ?>"></td>
              <td>Reception Last Name: </td>
              <td><input required type="text" name="ReceptionLastName" size="20"  value="<?php echo $receptionLastName; ?>"></td>
            </tr>
            <tr>
              <td colspan="4" style="text-align:center"><input type="submit" name="submit" value="Submit"></td>
            </tr>

          </table>
        </form>

      </div>
  </body>

</html>
