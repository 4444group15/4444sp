<?php
  session_start();
  // author: trentino longhi
  // define local vars to store form data entered by user

  $cardNum = "";
  $cardCVC = "";
  $cardFirstName = "";
  $cardLastName = "";
  $cardAddress = "";
  $paymentName = "";
  $err = "";

  if (isset($_POST["submit"])) { // checks whether submit was clicked
    if(isset($_POST["CardNum"])) $cardNum=$_POST["CardNum"];
    if(isset($_POST["CardCVC"])) $cardCVC=$_POST["CardCVC"];
    if(isset($_POST["CardFirstName"])) $cardFirstName=$_POST["CardFirstName"];
    if(isset($_POST["CardLastName"])) $cardLastName=$_POST["CardLastName"];
    if(isset($_POST["CardAddress"])) $cardAddress=$_POST["CardAddress"];
    if(isset($_POST["PaymentName"])) $paymentName=$_POST["PaymentName"];
    // return true if an empty string is returned

    if(// if everything is filled out
      !empty($cardNum) &&
      !empty($cardCVC) && ($cardCVC < 10000) &&
      !empty($cardFirstName) &&
      !empty($cardLastName) &&
      !empty($cardAddress) &&
      !empty($paymentName)) {
        header("HTTP/1.1 307 Temprary Redirect"); // needed for temp rediredirects, makes sure the second redirect will work
        header("Location: tManagePaymentSuccess.php"); // redirect to the success page
      } else { // if something goes wrong
        echo "PROBLEM";
        $err = true; // change err flag to true
        // it will automatically display error msgs we embedded throughout the doc
    } // end if form is filled out
  } // end if submit btn clicked
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

  <head>
    <!-- author - Trentino Longhi -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manage Payment</title>

    <!-- bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- set stylesheet -->
    <link rel="stylesheet" type="text/css" href="tStyle.css">

    <!-- nav bar style/jq -->
    <link rel="stylesheet" href="navbarstyles.css">
    <script type="text/javascript" src="navbarscript.js"></script>

  </head>

  <body>

    <!-- new nav bar -->
    <div class="navbar">
      <div class="topnav">
        <a href="#note" class="navbar-left"><img src="note.jpg" height="25"></a>
        <a class="active" href="vhome.php">Home</a>
        <a href="vhome.php#about">About</a>
        <a href="w_feedback.php">Contact</a>
        <a href="vComposerDashboard.php">My Dashboard</a>
        <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
      </div>
    </div>

    <!-- optional new nav bar which corresponds to correct user account type -->
    <!-- <div class="navbar">
      <div class="topnav">
        <a class="navbar-left"><img src="note.jpg" height="25"></a>
        <a class="active" href="vhome.php">Home</a>
        <a href="vhome.php#about">About</a>
        <a href="w_feedback.php">Contact</a>
        <?php
          // $type="";
          // $type=$_SESSION['acctype'];
          // $goto='';
          // if($type="Musician") $goto='vMusicDashboard.php';
          // if($type="Composer") $goto='vComposerDashboard.php';
          // if($type="Commissioner") $goto='vCommissionersDashboard.php';
        ?>
        <a href="<?php //echo $goto; ?>">My Dashboard</a>
        <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
      </div>
    </div> -->

    <!--table of all payment methods-->
    <div class="contentBox">
      <h2>Payment Method List</h2>
      <?php
        require_once("db.php");

        //send a query to the database
        $sql =
          "SELECT
            CardNum,
            concat(CardFirstName, ' ', CardLastName) AS CardName,
            PaymentName,
            CardAddress
          FROM
            dbpaymentmethod";
        // echo "$sql";
        $result = $mydb->query($sql);
        //$result should be a resultset

        // table header layout:
        echo "
          <body>
          <table>
          <thead class='outer'><tr>
            <th>Card Number</th>
            <th>Name on Card</th>
            <th>Payment Name</th>
            <th>Update?</th>
            <th>Delete?</th>
          </tr></thead>
        ";

        // table body layout and loop:
        while($row = mysqli_fetch_array($result)){
          // loop through all the rows in the result array
          echo "
            <tr>
             <td class='outer'>".$row["CardNum"]."</td>
             <td class='inner'>".$row["CardName"]."</td>
             <td class='inner'>".$row["PaymentName"]."</td>
             <td class='inner'><a href='tManagePaymentUpdate1.php?CardNum="
             .$row['CardNum']."&PaymentName="
             .$row['PaymentName']."&CardAddress="
             .$row['CardAddress']."'><button>update</button></a></td>
             <td class='inner'><a href='tManagePaymentDelete.php?CardNum="
             .$row['CardNum']."&CardName="
             .$row['CardName']."'><button>delete</button></a></td>
            </tr>
          ";
        } // end while loop for table body content

        echo "</table>";

      ?> <!-- end php -->
    </div> <!-- end table area -->

    <!-- add payment method form -->
    <div class="contentBox">
        <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>" >
          <h2>Add new payment method</h2>
          <table class="addNew">
            <tr>
              <!-- card num and cvc -->
              <td>Card Number: </td>
              <td><input required type="tel" name="CardNum" size="20"  value="<?php echo $cardNum; ?>"  maxlength="16"></td>
              <td>CVC: </td>
              <td><input required type="tel" name="CardCVC" size="4" value="<?php echo $cardCVC; ?>" maxlength="4"></td>
            </tr>
            <tr>
              <td>Create payment name: </td>
              <td><input required type="text" name="PaymentName" size="20" value="<?php echo $paymentName; ?>"></td>
              <td>Billing Address:</td>
              <td><input required type="text" name="CardAddress" size="20" value="<?php echo $cardAddress; ?>"></td>
            </tr>
            <tr>
              <td>Cardholder First Name: </td>
              <td><input required type="text" name="CardFirstName" size="20"  value="<?php echo $cardFirstName; ?>"></td>
                <td>Cardholder Last Name: </td>
                <td><input required type="text" name="CardLastName" size="20"  value="<?php echo $cardLastName; ?>"></td>
            </tr>
            <tr>
              <td colspan="4" style="text-align:center"><input type="submit" name="submit" value="Submit"></td>
            </tr>

          </table>
        </form>

      </div>
  </body>

</html>
