<!doctype html>
<html>
<head>
  <!-- author - Trentino Longhi -->
  <title>Reception addition success!</title>
</head>
<body>
  <?php
    session_start();
    $receptionAccount = "";
    $receptionName = "";
    $err = "";

    if(isset($_GET["ReceptionAccount"])) $receptionAccount=$_GET["ReceptionAccount"];
    if(isset($_GET["ReceptionName"])) $receptionName=$_GET["ReceptionName"];
    // hardcode - these would be (session vars)
    $memberID = 101;
    $memberType = 'musician';

    // add a new payment method
    require_once("db.php");
    $sql =
      "UPDATE
        dbreceptionmethod
      SET
        ReceptionName = '$receptionName'
      WHERE
        ReceptionAccount = '$receptionAccount'";
    $result=$mydb->query($sql);

    if ($result==1) { // if db table was modified
      echo "result: success! modified reception method $receptionName.<br><br>";
    } else { // if db table fialed to mod
      echo "result: failure. failed to modify reception method $receptionName.<br><br>";
    } // end result check
    // echo "$sql <br><br>"; // helps debugging
   ?>

   <!-- back button -->
  <br><a href="tManageReception.php"><button type="button" name="button">return to manage receptions page</button></a>

</body>
</html>
