<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<!-- test  -->
  <head>
    <!-- author - Trentino Longhi -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>View Owned Music Analytics</title>

    <!-- bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- set stylesheet -->
    <link rel="stylesheet" type="text/css" href="tStyle.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- nav bar style/jq -->
    <link rel="stylesheet" href="navbarstyles.css">
    <script type="text/javascript" src="navbarscript.js"></script>

    <!-- d3 lib/js -->
    <script src="https://d3js.org/d3.v4.js"></script>

  </head>

  <body>
    <!-- new nav bar -->
    <div class="navbar">
      <div class="topnav">
        <a href="#note" class="navbar-left"><img src="note.jpg" height="25"></a>
        <a class="active" href="vhome.php">Home</a>
        <a href="vhome.php#about">About</a>
        <a href="w_feedback.php">Contact</a>
        <a href="vCommissionersDashboard.php">My Dashboard</a>
        <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
      </div>
    </div>

    <!-- d3 graph -->
    <div class='contentBox' id="d3div">
      <h2>Density of song durations, in seconds</h2>
      <!-- nothing here, but it will be filled with the graph -->
    </div>
    <!-- d3 script -->
    <!-- base used: https://www.d3-graph-gallery.com/graph/density_basic.html -->
    <script>
      // set the dimensions and margins of the graph
      var margin = {top: 30, right: 30, bottom: 30, left: 50},
        width = 460 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;

      // append the svg object to the body of the page
      var svg = d3.select("#d3div")
      .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")");

      // get the data
      // d3.csv("https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/1_OneNum.csv", function(data) {
      d3.csv("tViewOwnedMusicAnalyticsGetData.php", function(data) {
        console.log(d3.max(data));

      // add the x Axis
      var x = d3.scaleLinear()
                .domain([0, 250])
                .range([0, width]);
      svg.append("g")
          .attr("transform", "translate(0," + height + ")")
          .call(d3.axisBottom(x));

      // add the y Axis
      var y = d3.scaleLinear()
                .range([height, 0])
                .domain([0, 0.05]);
      svg.append("g")
          .call(d3.axisLeft(y));

      // Compute kernel density estimation
      var kde = kernelDensityEstimator(kernelEpanechnikov(7), x.ticks(40));
      var density =  kde( data.map(function(d){  return d.recordingLength;  }) );
      //console.log(density);


      // Plot the area
      svg.append("path")
          .attr("class", "mypath")
          .datum(density)
          .attr("fill", "#69b3a2")
          .attr("opacity", ".8")
          .attr("stroke", "#000")
          .attr("stroke-width", 1)
          .attr("stroke-linejoin", "round")
          .attr("d",  d3.line()
            .curve(d3.curveBasis)
              .x(function(d) {
                // console.log(x(d[0]));
                return x(d[0]);
              })
              .y(function(d) {
                //console.log(y(d[1]));
                return y(d[1]);
               })
          );

      });


      // Function to compute density
      function kernelDensityEstimator(kernel, X) {
      return function(V) {
        return X.map(function(x) {
          return [x, d3.mean(V, function(v) { return kernel(x - v); })];
        });
      };
      }
      function kernelEpanechnikov(k) {
      return function(v) {
        return Math.abs(v /= k) <= 1 ? 0.75 * (1 - v * v) / k : 0;
      };
      }

      svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 0 - (width))
        .attr("x",0 - (height / 2))
        .attr("dy", "1em")
        .style("text-anchor", "middle")
        .text("Value");

    </script>

    <div class="">
      <!-- back button -->
     <br><a href="tViewOwnedMusic.php"><button type="button" name="button">return to Owned Music page</button></a>
    </div>
  </body>

</html>
