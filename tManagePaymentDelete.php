<!doctype html>
<html>
<head>
  <!-- author - Trentino Longhi -->
  <title>Payment deletion success!</title>
</head>
<body>
  <?php
    session_start();

    // get supplierid from the url
    $cardNum = 0;
    $cardName = "";
    if(isset($_GET["CardNum"])) $cardNum=$_GET["CardNum"];
    if(isset($_GET["CardName"])) $cardName=$_GET["CardName"];

    // remove a payment method
    require_once("db.php");
    $sql =
    "DELETE FROM
      dbpaymentmethod
    WHERE
      CardNum = $cardNum";
    // echo "$sql <br><br>"; // helps debugging
    $result = $mydb->query($sql);

    if ($result==1) {
      echo "result: success! deleted card $cardNum for $cardName.<br><br>";
    }else{
      echo "result: failure. failed to delete $cardNum for $cardName.<br><br>";
    } // end result check

   ?>

   <!-- back button -->
  <br><br><br><a href="tManagePayment.php"><button type="button" name="button">return to manage payments page</button></a>

</body>
</html>
