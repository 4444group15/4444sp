<!doctype html>
<html>
<head>
  <!-- author - Trentino Longhi -->
  <title>Payment update success!</title>
</head>
<body>
  <?php
    session_start();

    $cardNum = "";
    $cardAddress = "";
    $paymentName = "";
    $err = "";

    if(isset($_GET["CardNum"])) $cardNum=$_GET["CardNum"];
    if(isset($_GET["CardAddress"])) $cardAddress=$_GET["CardAddress"];
    if(isset($_GET["PaymentName"])) $paymentName=$_GET["PaymentName"];
    // hardcode - these would be (cookies?)
    $memberID = 101;
    $memberType = 'musician';
  ?>


  <?php
    // add a new payment method
    require_once("db.php");
    $sql =
      "UPDATE
        dbpaymentmethod
      SET
        CardAddress = '$cardAddress',
        PaymentName = '$paymentName'
      WHERE
        CardNum = '$cardNum'";
    $result=$mydb->query($sql);

    if ($result==1) { // if db table was modified
      echo "result: success! modified payment method $paymentName.<br><br>";
    } else { // if db table fialed to mod
      echo "result: failure. failed to modify payment method $paymentName.<br><br>";
    } // end result check
    // echo "$sql <br><br>"; // helps debugging
   ?>

   <!-- back button -->
  <br><a href="tManagePayment.php"><button type="button" name="button">return to manage payments page</button></a>

</body>
</html>
