<!DOCTYPE html>
<html lang="" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>View My Sheet Music</title>

    <!-- bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- set stylesheet -->
    <link rel="stylesheet" type="text/css" href="tStyles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- nav bar style/jq -->
    <link rel="stylesheet" href="navbarstyles.css">
    <script type="text/javascript" src="navbarscript.js"></script>

</head>

<style>
    h1{
      text-shadow: 3px 2px gray;
    }
    body {
    background-color: #F0FFFF;
    align-content: center;
    }
    button{
      background-color: white;
      color: black;
    }
    table.blueTable {
      border: 1px solid ;
      background-color: #FFE4C4;
      width: 80%;
      text-align: center;
      border-collapse: collapse;

    }
    table.blueTable td, table.blueTable th {
      border: 1px solid;
      padding: 3px 2px;
    }

    .inner{
    background-color: #FFFACD
    }
</style>

<body>
    <div class="navbar">
        <div class="topnav">
            <a href="#note" class="navbar-left"><img src="note.jpg" height="25"></a>
            <a class="active" href="#home">Home</a>
            <a href="#about">About</a>
            <a href="#contact">Contact</a>
            <a href="ComposerDashboard.php">My Dashboard</a>
            <div class="search-container">
                <form action="/action_page.php">
                    <input type="text" placeholder="Search.." name="search">
                    <button type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>

    <h1>Update Song Information</h1>
<?php
    $songName = "";
    $songInstrumentsList = "";
    $songDownloadsCount = "";
    $songPrice ="";
    $songRecordingsCount = "";
    $err = false;
    $nameErr = $instrumentErr = $songPriceErr = "";
    $comment = "";
    $SheetMusicID=0;

//INITIALIZE the local variables with the $_GET variables
if(isset($_GET['SheetMusicID'])) $sheetMusicID = $_GET['SheetMusicID'];
if(isset($_GET['songName'])) $songName = $_GET['songName'];
if(isset($_GET['songInstrumentsList'])) $songInstrumentsList = $_GET['songInstrumentsList'];
if(isset($_GET["songPrice"])) $songPrice=$_GET["songPrice"];


if (isset($_POST["update"])) {
    if(isset($_POST["songName"])) $songName=$_POST["songName"];
    if(isset($_POST["songInstrumentsList"])) $songInstrumentsList=$_POST["songInstrumentsList"];
    if(isset($_POST["songPrice"])) $songPrice=$_POST["songPrice"];
    if(!empty($songName) && !empty($songPrice) && !empty($songInstrumentsList)) {
        header("HTTP/1.1 307 Temprary Redirect"); //
        header("Location: jUpdateMusic.php");
    } else {
        $err = true;
    }
}
if (isset($_POST["delete"])) {
    if(isset($_POST["songName"])) $songName=$_POST["songName"];
    if(isset($_POST["songInstrumentsList"])) $songInstrumentsList=$_POST["songInstrumentsList"];
    if(isset($_POST["songPrice"])) $songPrice=$_POST["songPrice"];
echo $songPrice."+".$songName."+".$songInstrumentsList;
    if(!empty($songPrice) && !empty($songName) && !empty($songInstrumentsList)) {
      header("HTTP/1.1 307 Temprary Redirect"); //
      header("Location: jDeleteMusic.php");
    } else {
      $err = true;
    }
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["songName"])) {
    $nameErr = "Song Name is required";
  } else {
    $songName = test_input($_POST["songName"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$songName)) {
      $nameErr = "Only letters and white space allowed";
    }
  }
  
  if (empty($_POST["songInstrumentList"])) {
    $instrumentErr = "Instrument is required";
  } else {
    $songInstrumentsList = test_input($_POST["songInstrumentsList"]);
}
    
  if (empty($_POST["songPrice"])) {
    $songPriceErr = "Song Price is required";
  } else {
    $songPrice = test_input($_POST["songPrice"]);
    }

 

}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);

  return $data;
}
?>

<div>
<form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<p><span class="error">* required field</span></p>
  Song Name: <input type="text" name="songName" value="<?php echo $songName?>"/>
  <span class="error">* <?php echo $nameErr;?></span>
  <br><br>
  Instruments: <input type="text" name="songInstrumentsList" value="<?php echo $songInstrumentsList;?>">
  <span class="error">* <?php echo $instrumentErr;?></span>
  <br><br>
  Price: <input type="text" name="songPrice" value="<?php echo $songPrice;?>">
  <span class="error">*<?php echo $songPriceErr;?></span>
  <br><br>
  <br><br>
    <input type='hidden' name='SheetMusicID' value='<?php echo $sheetMusicID;?>'>
    <input type="submit" name = "update" value = "update" class="btn btn-primary">
    <br><br><a href="j_view_sheet_music.php"><button type="button" class="btn btn-primary" name="button">Return to view uploaded music page</button></a>
</form>
</div>

    
</body>

</html>