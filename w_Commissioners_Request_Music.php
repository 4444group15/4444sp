

<!-- need to get this from session cookie instead -->
<?php

session_start();
// if(isset($_SESSION['username'])) {
//   echo "Your session is running, " .$_SESSION['username']." and your ID is".$_SESSION['userid'];
// }



  //need to get the ID from the username table
  //then go to the Musician, Composer, or Commissioner table
  //and get the ID from there
  //$comID = $row["userID"];
//This page allows commissioners to submit a request

  $reqID = "";
  $comID = $_SESSION["userid"];
  $reqContent = "";
  $reqActive = 1;
  $reqSubmitDate = date("Y-m-d H:i:s");
  $err = false;

  if (isset($_POST["submit"])) {
      //if(isset($_POST["requestID"])) $reqID=$_POST["requestID"];
      //if(isset($_POST["commissionerID"])) $comID=$_POST["commissionerID"];
      if(isset($_POST["requestContent"])) $reqContent=$_POST["requestContent"];
      //if(isset($_POST["requestSubmitDate"])) $reqSubmitDate=$_POST["requestSubmitDate"];
      //if(isset($_POST["requestActive"])) $reqActive=$_POST["requestActive"];

      require_once("db.php");
      $reqID = "select Max(requestID) from dbrequest order by requestID desc";
      $result=$mydb->query($reqID);
      $row = mysqli_fetch_array($result);
      $newReqID=$row[0]+1;
      echo $newReqID;


      $sql = "insert into dbrequest values ('$newReqID','$comID','$reqContent','$reqSubmitDate',1)";
      echo $sql;

      $result=$mydb->query($sql);

      if ($result==1) {
        echo "<p>A new record has been created</p>";
      }

      if(!empty($reqID) && !empty($comID) && !empty($reqSubmitDate) && !empty($reqActive) && !empty($reqContent)) {
        header("HTTP/1.1 307 Temprary Redirect"); //
        header("Location: w_view_requests_commissioner_view.php");
      } else {
        $err = true;
      }


  }
 ?>

<!doctype html>
<html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Commission a work!</title>

      <!-- bootstrap -->
      <link href="css/bootstrap.min.css" rel="stylesheet" />
      <script src="jquery-3.1.1.min.js"></script>
      <script src="js/bootstrap.min.js"></script>

      <!-- set stylesheet -->
      <link rel="stylesheet" type="text/css" href="tStyles.css">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- nav bar style/jq -->
      <link rel="stylesheet" href="navbarstyles.css">
      <script type="text/javascript" src="navbarscript.js"></script>
      <style>.errlabel {color:red;}</style>
      <style>
      body{
        background-color: lightgray;
      }
      .floatingImg{
        float:left;
      }
      </style>
      <style>
        #animation {
          position: relative;
          -webkit-animation: spin linear 3s 0.2s 1 normal;
          animation: spin linear 3s 0.2s 1 normal;
        }

        @keyframes spin { 100% { -webkit-transform: rotate(360deg); transform:rotate(360deg); } }
        @-webkit-keyframes spin { 100% { -webkit-transform: rotate(360deg); } }
        /* @keyframes spin {
          0% {opactiy: 1; left: 0px; top: 0px; }
          25% {opacity: 1; left: 0px; top: 0px; }
          50% {opacity: ; left: 0px; top: 0px;}
          75% {opacity: 1; left: 0px; top: 0px;}
          100% {opacity: 1; left: 0px; top: 0px; } */


          /* 0% {opactiy: 1; left: 0px; top: 0px; }
          10% {opacity: 1; left: 50px; top: 50px; }
          20% {opacity: ; left: 100px; top: -50px;}
          30% {opacity: 1; left: 150px; top: 100px;}
          40% {opacity: 1; left: 200px; top: -50px; }
          50% {opacity: 1; left: 250px; top: 100px; }
          60% {opacity: 1; left: 300px; top: -50px; }
          70% {opacity: 1; left: 350px; top: 150px; }
          80% {opacity: 1; left: 400px; top: -50px; }
          90% {opacity: 1; left: 450px; top: 150px; }
          100% {opacity: 1; left: 500px; top: -50px; } */
        }

         @-webkit-keyframes spin {
           0% {opactiy: 1; left: 0px; top: 0px; }
           25% {opacity: 1; left: 0px; top: 0px; }
           50% {opacity: ; left: 0px; top: 0px;}
           75% {opacity: 1; left: 0px; top: 0px;}
           100% {opacity: 1; left: 0px; top: 0px; }

           /*
          0% {opactiy: 1; left: 0px; top: 0px; }
          10% {opacity: 1; left: 50px; top: 50px; }
          20% {opacity: ; left: 100px; top: -50px;}
          30% {opacity: 1; left: 150px; top: 100px;}
          40% {opacity: 1; left: 200px; top: -50px; }
          50% {opacity: 1; left: 250px; top: 100px; }
          60% {opacity: 1; left: 300px; top: -50px; }
          70% {opacity: 1; left: 350px; top: 150px; }
          80% {opacity: 1; left: 400px; top: -50px; }
          90% {opacity: 1; left: 450px; top: 150px; }
          100% {opacity: 1; left: 500px; top: -50px; } */
        }
      </style>

    </head>

<body>
  <div class="navbar">
    <div class="topnav">
      <a class="navbar-left"><img src="note.jpg" height="25"></a>
      <a class="active" href="vhome.php">Home</a>
      <a href="vhome.php#about">About</a>
      <a href="w_feedback.php">Contact</a>
      <a href="vCommissionersDashboard.php">My Dashboard</a>
      <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
    </div>
<div class="floatingImg">
  </div>

  <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">


    <!-- <label>Request ID:</label>
    <input name="requestID" type="number" value="<?php //echo $reqID; ?>"/>
    <?php
    //  if ($err && $reqID=="") {
      //  echo "<label class='errlabel'>Error: Please enter a commissioner name</label>";
    //  }
    ?>
  </br> -->
  <h1 style="margin-left:20px">Request Music</h1>
  <p style="margin-left:20px; width: 700px">Commissioner accounts may use this form to request new music from composers. Please describe the piece you would like written. Be sure to include what instruments or voice types, the style, the desired length if known, and any other relevant details. Our system cannot read apostrophes.</p>





    <label style="margin-left:20px">Request Content:</label>
    <br/>
      <!-- <input style="width: 300px; height: 200px; vertical-align: top; padding: 10px 0 200px 0;" name="requestContent" type="textarea" value="<?php //echo $reqContent; ?>"/> -->
      <div style="margin-left:20px">


      <textarea name="requestContent" rows="8" cols="95" value="<?php echo $reqContent; ?>"></textarea>

      <?php
        if ($err && empty($reqContent)) {
          echo "<label class='errlabel'>Error: Please enter a request description</label>";
        }
      ?>
      </div>
    <br />



    <!-- <label>Request Submit Date:</label>
    <input type="text" name="requestSubmitDate" value="<?php //echo $reqSubmitDate; ?>"/>
    <?php
    //  if ($err && empty($reqSubmitDate)) {
      //  echo "<label class='errlabel'>Error: Please enter a valid unit price</label>";
      //}
    ?>
    <br /> -->



    <!-- <label>Request Active:</label>
    <input type="text" name="requestActive" value="<?php //echo $reqActive; ?>"/>
    <?php
  //    if ($err && empty($reqActive)) {
    //    echo "<label class='errlabel'>Error: Please enter a 1</label>";
    //  }
    ?>
    <br /> -->

    <input style="margin-left:20px" type="submit" name="submit" value="Submit" />
    <br />
    <!-- <input style="margin-left:80px" type="submit" class="button" onclick="document.location.href='w_instruments.php';" name="pairings" value="Which instruments pair well together?" /> -->

  </form>

<?php


      // require_once("db.php");
      // $sql = "insert into dbrequest values ('$reqID','$comID','$reqContent','$reqSubmitDate','$reqActive')";
      // echo $sql;
      //
      // $result=$mydb->query($sql);
      //
      // if ($result==1) {
      //   echo "<p>A new record has been created</p>";
      // }


?>

<br/><br/>

<input style="margin-left:20px" type="submit" class="button" onclick="document.location.href='w_instruments.php';" name="pairings" value="Which instruments pair well together?" />
<div>
<div class="floatingImg">
<img STYLE="position:absolute; TOP:250px; LEFT:850px;" id="animation" src="./images/notes.png" alt="" height="100px" width="300px">
</div>
</body>
</html>
