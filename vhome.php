<!DOCTYPE html>
<html>
<title>CrowdChords</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body>

<!-- Navbar (sit on top) -->
<div class="w3-top">
  <div class="w3-bar w3-white w3-wide w3-padding w3-card">
    <a href="#home" class="w3-bar-item w3-button"><b>CrowdChords</b> </a>
    <!-- Float links to the right. Hide them on small screens -->
    <div class="w3-right w3-hide-small">
         <a href="uloginpage.php" class="w3-bar-item w3-button">Sign In</a>
    </div>
  </div>
</div>

<!-- Header -->
<header class="w3-display-container w3-content w3-wide" style="max-width:1500px;" id="home">
  <img class="w3-image" src="music.jpg" alt="Architecture" width="1500" height="800">
  <div class="w3-display-middle w3-margin-top w3-center">
    <h1 class="w3-xxlarge w3-text-white"><span class="w3-padding w3-black w3-opacity-min"><b>CrowdChords</b></span> <span class="w3-hide-small w3-text-light-grey"></span></h1>
  </div>
</header>

<!-- Page content -->
<div class="w3-content w3-padding" style="max-width:1564px">



  <!-- About Section -->
  <div class="w3-container w3-padding-32" id="about">
    <h3 class="w3-border-bottom w3-border-light-grey w3-padding-16">About</h3>
    <p> Post, review, sell, and purchase music from CrowdChords. CrowdChords helps composers sell their music, musicians upload their songs, and commisssioners buy music that they need. Our website allows you to get an insight in the world of music while also getting paid.
    </p>
    <p>The process goes something like this: <br/>
    A composer writes a new piece of music but doesn't have the funding to make a professional recording (which helps them sell their pieces to large companies like HalLeonard and JW Pepper). The composer comes to CrowdChords and uploads the sheet music. Other CrowdChords users, a.k.a. Musicians, read the different instrumental or vocal parts of that sheet music and make a recording.
    Once the composer has enough recordings of each part, he or she can mix them to make a recording which they can submit to sheet music companies.
  </p>

  <p>Additionally, outside companies and other parties who want a new piece of music written for them can commission one. These users are called Commissioners.
    Commissioners make requests, which composers can view and fulfill - Commissioners then sort through the composers' submissions and  choose their favorite song or jingle to buy from the composer.
  </p>
  <p>
    CrowdChords is a welcoming space designed to help composers who can't afford professional musicians tap into the power of crowdsourcing. Welcome to CrowdChords!
  </p>

  </div>


<!-- End page content -->
</div>


<!-- Footer -->
<footer class="w3-center w3-black w3-padding-16">
  <p>Powered by CrowdChords</p>
</footer>

</body>
</html>
