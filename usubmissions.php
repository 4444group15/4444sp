<?php

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Active request</title>

    <!-- bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- set stylesheet -->
    <link rel="stylesheet" type="text/css" href="tStyles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- nav bar style/jq -->
    <link rel="stylesheet" href="navbarstyles.css">
    <script type="text/javascript" src="navbarscript.js"></script>

  </head>

  <body>
    <!-- new nav bar -->
    <div class="navbar">
      <div class="topnav">
        <a href="#note" class="navbar-left"><img src="note.jpg" height="25"></a>
        <a class="active" href="#home">Home</a>
        <a href="#about">About</a>
        <a href="#contact">Contact</a>
        <div class="search-container">
          <form action="/action_page.php">
            <input type="text" placeholder="Search.." name="search">
            <button type="submit">Submit</button>
          </form>
        </div>
      </div>
    </div>

    <!-- input search box -->
    <div class="contentBox">
      <input type="text" name="inputSearchTerm" placeholder="[enter search terms]" style="width:80%">
      <button type="button" name="button">Search</button>
    </div>

    <!-- new search results -->
    <div class="contentBox">
      <?php
        require_once("db.php");

        // temp harcode - will be a (cookie?) later
        $memberID = 3;

        //send a query to the database
        $sql =
          "SELECT
            requestID,
            concat(commissionerFirstName, ' ', commissionerLastName) AS commissionerName,
            requestContent
          FROM
            dbrequest rq, dbcommissioner c
          WHERE
            rq.commissionerID = c.MemberID
            AND requestActive=0";
        // echo $sql;
        $result = $mydb->query($sql);

        // table styles
        echo"<style>";

          // first column/row:
          echo ".outer {
            // background-color: #202020;
            // color: white;
            // font-weight: bold;
            padding: 7px;
            }";
          // body content:
          echo ".inner {
            // background-color: #D3D3D3;
            // color: black;
            }";
        echo "</style>";

        // table header layout:
        echo "
          <body>
          <table>
          <thead class='outer'><tr>
            <th>Request ID</th>
            <th>Name</th>
            <th>Details</th>
            <th>Create</th>
            <th>View Sheet Music</th>
          </tr></thead>
        ";

        // table body layout and loop:
        while($row = mysqli_fetch_array($result)){
          // loop through all the rows in the result array
          echo "
            <tr>
              <td class='outer'>".$row["requestID"]."</td>
              <td class='inner'>".$row["commissionerName"]."</td>
              <td class='inner'>".$row["requestContent"]."</td>
              <td class='inner'><button onclick=\"location.href='index.html'\">Make</button></td>
              <td class='inner'><button onclick=\"location.href='index.html'\">music</button></td>
            </tr>
          ";
          // <td class='inner'><a href='".$row["recordingUrl"]."'>link</a></td>
        } // end while loop for table body content
        echo "</table>"
      ?>
    </div> <!-- end table area -->

  </body>

  </html>
