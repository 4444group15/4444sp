<!doctype html>
<html>
<head>
  <!-- author - Trentino Longhi -->
  <title>Reception deletion success!</title>
</head>
<body>
  <?php
    session_start();

    // get supplierid from the url
    $receptionAccount = "";
    $receptionName = "";
    if(isset($_GET["ReceptionAccount"])) $receptionAccount=$_GET["ReceptionAccount"];
    if(isset($_GET["ReceptionUsername"])) $receptionName=$_GET["ReceptionUsername"];

    // remove a payment method
    require_once("db.php");
    $sql =
    "DELETE FROM
      dbreceptionmethod
    WHERE
      ReceptionAccount = '$receptionAccount'";
    // echo "$sql <br><br>"; // helps debugging
    $result = $mydb->query($sql);

    if ($result==1) {
      echo "result: success! deleted card $receptionAccount for $receptionName.<br><br>";
    }else{
      echo "result: failure. failed to delete $receptionAccount for $receptionName.<br><br>";
    } // end result check

   ?>

   <!-- back button -->
  <br><br><br><a href="tManageReception.php"><button type="button" name="button">return to manage receptions page</button></a>

</body>
</html>
