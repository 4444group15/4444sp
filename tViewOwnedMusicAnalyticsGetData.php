<?php
  session_start();
  // author: Trentino Longhi
  require_once("db.php");

  $sql =
    "SELECT
      recordingLength
    FROM
      dbrecording;
    ";

  // execute the sql
  $result = $mydb->query($sql);
  // $str_time = 0;
  echo "recordingLength";

  // print each mm:ss time as seconds only
  while($row=mysqli_fetch_array($result)) {
    // parse method based on https://stackoverflow.com/a/4834230
    $str_time = $row['recordingLength'];
    $str_time = preg_replace(
      "/^([\d]{1,2})\:([\d]{2})$/",
      "00:$1:$2",
      $str_time);
    sscanf($str_time, '%d:%d', $minutes, $seconds);
    $time_seconds = $minutes * 60 + $seconds;
    echo "\n$time_seconds";
  }

 ?>
