<?php
//this page is the composer view
//allows composers to access the recordings uploaded by musicians
$Recording_Search= "";
$Item_Desc = "";

$compID='';
session_start();
$compID=$_SESSION['userid'];
?>
 <!DOCTYPE html>
 <html lang="" dir="ltr">
   <head>
     <head>
       <meta charset="utf-8">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <title>Access Recordings</title>

       <!-- bootstrap -->
       <link href="css/bootstrap.min.css" rel="stylesheet" />
       <script src="jquery-3.1.1.min.js"></script>
       <script src="js/bootstrap.min.js"></script>

       <!-- set stylesheet -->
       <link rel="stylesheet" type="text/css" href="tStyles.css">
       <meta name="viewport" content="width=device-width, initial-scale=1">

       <!-- nav bar style/jq -->
       <link rel="stylesheet" href="navbarstyles.css">
       <script type="text/javascript" src="navbarscript.js"></script>
       <style media="screen">
       table {
         background-color: #95B9C7;
         margin-left: 20px;

       }
       th{
         background-color: darkgray;
         color: white;
       }

       </style>

     </head>


   <body>
     <div class="navbar">
       <div class="topnav">
         <a href="#note" class="navbar-left"><img src="note.jpg" height="25"></a>
         <a class="active" href="vhome.php">Home</a>
         <a href="vhome.php#about">About</a>
         <a href="w_feedback.php">Contact</a>
         <a href="vComposerDashboard.php">My Dashboard</a>
         <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
       </div>
     </div>

     <h1 style="margin-left: 20px;">Recordings Submitted by Musicians</h1>

     <table border="1px">

         <tr>
         <th>
           Recording ID:
         </th>
         <th>
           Recording Name:
         </th>
         <th>
           Album Name:
         </th>
         <th>
           Musician ID:
         </th>
         <th>
           Submission Date:
         </th>
         <th>
           Recording Length:
         </th>
         <th>
           Recording URL:
         </th>
       </tr>
       <tr>
         <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>">


           <label style="margin-left: 20px;"">Recording Search:</label>
           <input name="Recording_Search" type="text" value="<?php echo $Recording_Search; ?>"/>
           <input type="submit" name="Search" value="Search">
           <br />
           <br />
        </form>

          <?php
          require_once("db.php");

           //if (isset($_POST["number"])) {
           if(isset($_POST["Recording_Search"])) {$Recording_Search=$_POST["Recording_Search"];
           //$Recording_Search="number";


           $sql = "select * from dbrecording where recordingalbum like '%$Recording_Search%'
           OR recordingname like '%$Recording_Search%'
           OR recordingid like '%$Recording_Search%'
           OR recordingsubmitdate like '%$Recording_Search%'
           OR recordingmusicianid like '%$Recording_Search%'
           OR recordingLength like '%$Recording_Search%'
           OR recordingUrl like '%$Recording_Search%'";
         }
            //SELECT * FROM 4444 WHERE recordingID LIKE ? select * FROM dbrecording WHERE recordingID=$Recording_Search
           if($Recording_Search==""){
             $sql = "select * FROM dbrecording";}

           $result = $mydb->query($sql);

           // $oneArray = Array();
           // $twoArray = Array();
           // $threeArray = Array();
           // $fourArray = Array();
           // $fiveArray = Array();
           // $sixArray = Array();
           // $sevenArray = Array();
           //echo "<table border='1px'>";
           while ($row = mysqli_fetch_array($result)) {
             echo "
               <tr>
                <td class='recordingID'>".$row["recordingID"]."</td>
                <td class='rName'>".$row["recordingName"]."</td>
                <td class='rAlbum'>".$row["recordingAlbum"]."</td>
                <td class='rMusicianID'>".$row["recordingMusicianID"]."</td>
                <td class='rSubmitDate'>".$row["recordingSubmitDate"]."</td>


                <td class='player'><audio id='audio' controls volume='0.7'><source src='".$row["recordingUrl"]."' type='audio/wav'></audio></td>
                <td class='inner'><a href='".$row["recordingUrl"]."' download='".$row["recordingName"]."'><button>Download</button></a></td>
               </tr>
             ";
             //<td class='rUrl'>".$row["recordingUrl"]."</td>
             // <td class='rLength'>".$row["recordingLength"]."</td>
             //
           } // end while loop for table body content
           echo "</table>"

            ?>

       </tr>
       <div style="padding-bottom:50px;"></div>
   </body>
 </html>
