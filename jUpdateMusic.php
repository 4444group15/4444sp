<!doctype html>
<html>
<head>
  <title>Successfully Updated Sheet Music!</title>
</head>
<body>
  <?php

    $songName = "";
    $songInstrumentsList = "";
    $songPrice =0;
    $SheetMusicID ="";
    // Use post
    if(isset($_POST["songName"])) $songName=$_POST["songName"];
    if(isset($_POST["songInstrumentsList"])) $songInstrumentsList=$_POST["songInstrumentsList"];
    if(isset($_POST["songPrice"])) $songPrice=$_POST["songPrice"];
    if(isset($_POST["SheetMusicID"])) $SheetMusicID=$_POST["SheetMusicID"];

  ?>
  <?php
    // update database record
    require_once("db.php");
    $sql =
      "UPDATE
        dbsheetmusic
      SET
        songName = '$songName',
        songInstrumentsList= '$songInstrumentsList',
        songPrice= '$songPrice'
      WHERE
        SheetMusicID = $SheetMusicID

        ";
    
    $result=$mydb->query($sql);

    if ($result==1) { // if db table was modified
      echo "Result: Successfully modified sheet music information for song: $songName.<br><br>";
    } else { // if db table failed to mod
      echo "Result: Failed to modify sheet music informaiton for song: $songName.<br><br>";
    } // end result check
    // echo "$sql <br><br>"; // helps debugging
   ?>
   <br><br><a href="j_view_sheet_music.php"><button type="button" class="btn btn-primary" name="button">Return to view uploaded music page</button></a>
</body>
</html>
