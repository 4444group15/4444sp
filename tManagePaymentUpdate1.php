<!doctype html>
<html>
<head>
  <!-- author - Trentino Longhi -->
  <title>Payment update success!</title>
</head>
<body>
  <?php
    session_start();

    $cardNum = "";
    $cardAddress = "";
    $paymentName = "";
    $err = "";

    if(isset($_GET["CardNum"])) $cardNum=$_GET["CardNum"];
    if(isset($_GET["CardAddress"])) $cardAddress=$_GET["CardAddress"];
    if(isset($_GET["PaymentName"])) $paymentName=$_GET["PaymentName"];
    // hardcode - these would be (session vars) in actuality
    $memberID = 101;
    $memberType = 'musician';

    // add a new payment method
    require_once("db.php");
    $sql =
      "SELECT
        CardNum,
        concat(CardFirstName, ' ', CardLastName) AS CardName,
        PaymentName,
        CardAddress
      FROM
        dbpaymentmethod
      WHERE
        CardNum = '$cardNum'";

    //format
    echo "
    <style>
      table, th, td {
        border:1px solid black;
      }
    </style>
    ";
    // table header layout:
    echo "
      <form method='get' action='tManagePaymentUpdate2.php?'>
        <input type='hidden' name='CardNum' value='$cardNum'></input><br>
          Card Address: <input type='text' name='CardAddress' value='$cardAddress'></input><br>
          Payment Method Name: <input type='text' name='PaymentName' value='$paymentName'></input><br>
        <input type='submit' name='submit' value='Submit'></input>
      </form>
    ";
   ?>

   <!-- back button -->
  <br><br><br><a href="tManagePayment.php"><button type="button" name="button">return to manage payments page</button></a>

</body>
</html>
