<?php
//View the sheet music uploaded by composers
//need session

$Request_Search= "";
$Sold_or_Not="";

$compID="";
session_start();
$compID=$_SESSION['userid'];
// echo $compID;

?>
 <!DOCTYPE html>
 <html lang="" dir="ltr">
   <head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>View My Sheet Music</title>

     <!-- bootstrap -->
     <link href="css/bootstrap.min.css" rel="stylesheet" />
     <script src="jquery-3.1.1.min.js"></script>
     <script src="js/bootstrap.min.js"></script>

     <!-- set stylesheet -->
     <link rel="stylesheet" type="text/css" href="tStyles.css">
     <meta name="viewport" content="width=device-width, initial-scale=1">

     <!-- nav bar style/jq -->
     <link rel="stylesheet" href="navbarstyles.css">
     <script type="text/javascript" src="navbarscript.js"></script>

     <style media="screen">
     body{
       background-color: lightgray;
     }
       th{
         background-color: steelblue;
         color:white;
       }
       /* .formButtons{
         float:left;
       } */
     </style>

   </head>
   <body>
     <div class="navbar">
       <div class="topnav">
         <a class="navbar-left"><img src="note.jpg" height="25"></a>
         <a class="active" href="vhome.php">Home</a>
         <a href="vhome.php#about">About</a>
         <a href="w_feedback.php">Contact</a>
         <a href="vComposerDashboard.php">My Dashboard</a>
         <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
       </div>
     </div>

     <h1 style="margin-left:20px">View My Sheet Music</h1>
     <table border="1px" style="margin-left:20px;float:left;background-color:white;">
         <tr>
         <th>
           Song Name:
         </th>
         <th>
            Instruments List:
         </th>
         <th>
           Download Count:
         </th>
         <th>
           Price:
         </th>
         <th>
           Recordings Count:
         </th>
         <th>
           Download:
         </th>
       </tr>
       <tr>
         <form method="post" action="<?php echo $_SERVER['PHP_SELF']?>" />

           <div class="formButtons">
           <label style="margin-left:20px">Search Terms:</label>
           <input style="margin-left:21px" name="Request_Search" type="text" value="<?php echo $Request_Search; ?>"/>
           <br/>
           <label style="margin-left:20px">Sold or Not Sold:</label>
           <select style="margin-left:3px" name="soldYN">
             <option value="all" selected></option>
             <option value="sold">sold</option>
             <option value="not sold">not sold</option>
           </select>
         </div>


         <div class="formButtons">
           <input style="margin-left:140px" type="submit" name="Search" value="Search">
         </div>
           <br />
        </form>

          <?php
          require_once("db.php");
          if(isset($_POST['soldYN'])) {$Sold_or_Not=$_POST['soldYN'];}
          else {$Sold_or_Not='all';}

           //if (isset($_POST["number"])) {
           if(isset($_POST["Request_Search"])) {$Request_Search=$_POST["Request_Search"];}
           else {$Request_Search='';}


           if($Sold_or_Not=='all'){
            $sql = "select * from dbsheetmusic where
            songname like '%$Request_Search%'
            OR songinstrumentslist like '%$Request_Search%'
            OR songdownloadscount like '%$Request_Search%'
            OR songprice like '%$Request_Search%'
            OR songrecordingscount like '%$Request_Search%'
            AND (composerID='$compID')
            ";}

           elseif($Sold_or_Not=='not sold'){
           $sql = "select * from dbsheetmusic where (soldYN=0) AND
           (songname like '%$Request_Search%'
           OR songinstrumentslist like '%$Request_Search%'
           OR songdownloadscount like '%$Request_Search%'
           OR songprice like '%$Request_Search%'
           OR songrecordingscount like '%$Request_Search%')
           AND (composerID=$compID)
           ";}

           elseif($Sold_or_Not=='sold'){
           $sql = "select * from dbsheetmusic where (soldYN=1) AND
           (songname like '%$Request_Search%'
           OR songinstrumentslist like '%$Request_Search%'
           OR songdownloadscount like '%$Request_Search%'
           OR songprice like '%$Request_Search%'
           OR songrecordingscount like '%$Request_Search%')
           AND (composerID=$compID)
           ";}


           if($Request_Search==""){
             if($Sold_or_Not=='all'){
              $sql = "select * from dbsheetmusic where composerID=$compID";}

             elseif($Sold_or_Not=='not sold'){
             $sql = "select * from dbsheetmusic where (soldYN=0) and (composerID=$compID)";}

             elseif($Sold_or_Not=='sold'){
             $sql = "select * from dbsheetmusic where (soldYN=1) and (composerID=$compID)";}}

           $result = $mydb->query($sql);

           while ($row = mysqli_fetch_array($result)) {
             echo "
               <tr>
                <td class='rName'><strong><a href='w_access_recordings.php'>".$row["songName"]."</a></strong></td>
                <td class='rAlbum'>".$row["songInstrumentsList"]."</td>
                <td class='rMusicianID'>".$row["songDownloadsCount"]."</td>
                <td class='rSubmitDate'>".$row["songPrice"]."</td>
                <td class='rLength'>".$row["songRecordingsCount"]."</td>
                <td class='inner'><a href='".$row["sheetMusicUrl"]."' download='".$row["songName"]."'><button>Download</button></a></td>
               </tr>

             ";
           } // end while loop for table body content
           //<td class='inner'><a href='".$row["sheetMusicUrl"]."' download='".$row["songName"]."'><button>Download</button></a></td>
           echo "</table>"

           // $oneArray = Array();
           // $twoArray = Array();
           // $threeArray = Array();
           // $fourArray = Array();
           // $fiveArray = Array();
           // while ($row = mysqli_fetch_array($result)) {
              // $oneArray[] =  $row['songName'];
              // $twoArray[] = $row['songInstrumentsList'];
              // $threeArray[] = $row['songDownloadsCount'];
              // $fourArray[] = $row['songPrice'];
              // $fiveArray[] = $row['songRecordingsCount'];}
          // echo "<td>".implode("</br>", $oneArray)."</td>";
          // echo "<td>".implode("</br>", $twoArray)."</td>";
          // echo "<td>".implode("</br>", $threeArray)."</td>";
          // echo "<td>".implode("</br>", $fourArray)."</td>";
          // echo "<td>".implode("</br>", $fiveArray)."</td>";
            ?>
       </tr>
       <?php end: ; ?>


   </body>
 </html>
