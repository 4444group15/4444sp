<?php
$firstName="";
$lastName="";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
        .errlabel {color:red};
        .center {
          margin: auto;
        width: 50%;
        border: 3px solid green;
        padding: 10px;
        display: inline-block;
        background: @blue;
        border: 1px solid darken(@blue, 5%);
        padding: .5em 2em;
        color: white;
        margin-right: .5em;
        box-shadow: inset 0px 1px 0px;
 }



 html, body {
     width: 100%;
     background-color: white;
     margin: auto;
   width: 50%;
    background-image: url("mic.jpg");


 }
          form {
    display: block;
    width: 100%;
    padding: 2em;
  }
  input {
   display: block;
   margin: auto auto;
   width: 100%;
   margin-bottom: 2em;
   padding: .5em 0;
   border: none;
   border-bottom: 1px solid #eaeaea;
   padding-bottom: 1.25em;
   color: #757575;}
   h1{
color: black;

margin-right: auto;
margin-top: auto;
margin-bottom: auto;

   }
   div {


     background-color: #ddd;
     color:#ddd ;
     padding: 20px;
     margin: auto;
     width: 75%;
   }

   td{
     color: black;

   }
   .btn {
    display: inline-block;
    background-color:white;


    padding: .5em 2em;
    color: blue;
    margin-right: .5em;}
    @grey:#2a2a2a;
@blue:#1fb5bf;
.tiny {font-size: 12px;
color: white;}
#white{color: white;}
    </style>
  </head>

  <?php
  $fname = "";
  $lname = "";
  $accType = "";
    $username="";
    $password="";
    $remember="no";
    $error = false;
    $loginOK = null;
    $err2 = false;


    if(isset($_POST["submit"])){
      if(isset($_POST["username"])) $username=$_POST["username"];
      if(isset($_POST["pwd"])) $password=$_POST["pwd"];
      if(isset($_POST["remember"])) $remember=$_POST["remember"];

      if(empty($username) || empty($password)) {
        $error=true;
      }

      if(!empty($password) && $remember=="yes"){
        setcookie("psswrd", $password, time()+60*60*24*2, "/");
        setcookie("username", $username, time()+60*60*24*2, "/");
        date_default_timezone_set("America/New_York");
        setcookie("last", date("m,d,y,h:i:s"), time()+60*60*24*2, "/");
      }



            if(!$error){
              require_once("db.php");
              $sql = "select userid, firstName, lastName, psswrd, username, acctype from dblogin where username='$username'";

              $result = $mydb->query($sql);

              $row=mysqli_fetch_array($result);
              if ($row){
                if(strcmp($password, $row["psswrd"]) ==0 && strcmp($username, $row["username"])==0 ){
                  $loginOK=true;
                  $firstName=$row['firstName'];
                  $lastName=$row['lastName'];
                  $userid=$row['userid'];
                } else {
                  $loginOK = false;
                }
              }

              if($loginOK) {

                session_start();

                $_SESSION["username"] = $username;
                $_SESSION['firstName']=$firstName;
                $_SESSION['lastName']=$lastName;
                $_SESSION['userid']=$userid;

                $acctype = $row["acctype"];
                $_SESSION['acctype']=$acctype;
                if(strcmp("Musician", $row["acctype"]) == 0){

                Header("Location:vMusicDashboard.php");
              }
                else if(strcmp("Composer", $row["acctype"])==0){

                Header("Location:vComposerDashboard.php");
              }
                else{

                    Header("Location:vCommissionersDashboard.php");
                }
        }

      }

    }
    if(isset($_POST["update"])){
      if(isset($_POST["username"])) $username=$_POST["username"];
      if(isset($_POST["pwd"])) $password=$_POST["pwd"];
      if(isset($_POST["remember"])) $remember=$_POST["remember"];

      if(empty($username) || empty($password)) {
        $error=true;
      }

      if(!empty($password) && $remember=="yes"){
        setcookie("psswrd", $password, time()+60*60*24*2, "/");
        setcookie("username", $username, time()+60*60*24*2, "/");
        date_default_timezone_set("America/New_York");
        setcookie("last", date("m,d,y,h:i:s"), time()+60*60*24*2, "/");
      }



            if(!$error){
              require_once("db.php");
              $sql = "select userid, firstName, lastName, psswrd, username, acctype from dblogin where username='$username'";

              $result = $mydb->query($sql);

              $row=mysqli_fetch_array($result);
              if ($row){
                if(strcmp($password, $row["psswrd"]) ==0 && strcmp($username, $row["username"])==0 ){
                  $loginOK=true;
                  $firstName=$row['firstName'];
                  $lastName=$row['lastName'];
                  $userid=$row['userid'];
                } else {
                  $loginOK = false;
                }
              }

              if($loginOK) {

                session_start();

                $_SESSION["username"] = $username;
                $_SESSION['firstName']=$firstName;
                $_SESSION['lastName']=$lastName;
                $_SESSION["userid"]=$userid;


                    Header("Location:uprofileupdate.php");
                }
        }

      }
      if(isset($_POST["delete"])){
        if(isset($_POST["username"])) $username=$_POST["username"];
        if(isset($_POST["pwd"])) $password=$_POST["pwd"];
        if(isset($_POST["remember"])) $remember=$_POST["remember"];

        if(empty($username) || empty($password)) {
          $error=true;
        }

        if(!empty($password) && $remember=="yes"){
          setcookie("psswrd", $password, time()+60*60*24*2, "/");
          setcookie("username", $username, time()+60*60*24*2, "/");
          date_default_timezone_set("America/New_York");
          setcookie("last", date("m,d,y,h:i:s"), time()+60*60*24*2, "/");
        }



              if(!$error){
                require_once("db.php");
                $sql = "select userid, firstName, lastName, psswrd, username, acctype from dblogin where username='$username'";

                $result = $mydb->query($sql);

                $row=mysqli_fetch_array($result);
                if ($row){
                  if(strcmp($password, $row["psswrd"]) ==0 && strcmp($username, $row["username"])==0 ){
                    $loginOK=true;
                    $firstName=$row['firstName'];
                    $lastName=$row['lastName'];
                    $userid=$row['userid'];
                  } else {
                    $loginOK = false;
                  }
                }

                if($loginOK) {

                  session_start();

                  $_SESSION["username"] = $username;


                      Header("Location:uprofiledeleted.php");
                  }
          }

        }


   ?>
  <body>

    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
<div>
<h1>
   Login
</h1>

      <table class="center">
        <tr>
          <td>Username</td>
        </tr>
        <tr>
          <td><input type="text" name="username" value="<?php
            if(!empty($username))
              echo $username;
            else if(isset($_COOKIE['username'])) {
              echo $_COOKIE['username'];
            }
          ?>" /><?php if($error && empty($username)) echo "<span class='errlabel'> please enter a username</span>"; ?></td>
        </tr>
        <tr>
          <td>Password</td>
        </tr>
        <tr>
          <td><input type="Password" name="pwd" value="<?php
          if(!empty($password))
            echo $password;
          else if(isset($_COOKIE['psswrd'])) {
            echo $_COOKIE['psswrd'];
          } ?>" /><?php if($error && empty($password)) echo "<span class='errlabel'> please enter a password</span>";
          ?></td>
        </tr>
      </table>


      <table>
        <tr>

          <td>
          <label>Remember me</label>
          <input type="checkbox" name="remember" value="yes"/></td>
        </tr>
        <tr>
        <td>
        <p>
          Don't have an account? <a href="usignup.php">Sign Up</a>
        </p>
        </td>
        </tr>
        <tr>
          <td><?php if(!is_null($loginOK) && $loginOK==false) {echo "<span class='errlabel'>username and password do not match.</span>";} ?></td>
        </tr>
        <tr>
          <td><input class="btn" type="submit" name="submit" value="Login" /></td>


        </tr>
        <tr>
        <td><input class="btn" type="submit" name="update" value="Update Profile" /> <input class="btn" type="submit" name="delete" value="Delete Profile" /></td>

        </tr>



      </table>



      </div>

    </form>

</div>
<p class = "tiny">
Acount Analytics<a id = "white" href="uaccountgraph.php"> See Graph </a>
</p>


  <!-- trentino added this right before submitting,
    if something looks wrong don't take off from usman's grade -->
  <div style="color:black;padding:2px;text-align:center;">
    <h2>login info:</h2>
    <table style="margin:0 auto;">
      <thead>
        <th>acct type</th>
        <th>username</th>
        <th>pasword</th>
      </thead>
      <tbody>
        <tr>
          <td>commissioner</td>
          <td>quinn</td>
          <td>1234</td>
        </tr>
        <tr>
          <td>musician</td>
          <td>joshua</td>
          <td>1234</td>
        </tr>
        <tr>
          <td>composer</td>
          <td>hannah</td>
          <td>1234</td>
        </tr>
      </tbody>
    </table>
  </div> <!-- end trentino's addition -->



  </body>
</html>
