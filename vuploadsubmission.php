<?php
  session_start();
  $productID = 0;
  $firstname = "";
  $lastname = "";
  $filename = "";
  $productName = "";
  $supplierID = "";
  $name_file = "";
  $tmp_name = "";
  $local_image = "submissions/";

  $err = false;

  if (isset($_POST["submit"])) {
    if(isset($_POST["productID"])) $productID = $_POST["productID"];
      if(isset($_POST["firstname"])) $firstname = $_POST["firstname"];
        if(isset($_POST["lastname"])) $lastname = $_POST["lastname"];
          if(isset($_POST["filename"])) $filename = $_POST["filename"];
    //if(isset($_POST["file"])) $productName = $_POST["file"];
    $name_file = $_FILES['file']['name'];
    $tmp_name = $_FILES['file']['tmp_name'];


    if (!empty($tmp_name)) {
      require_once("db.php");

        $sql = "insert into dbsubmission(firstname,lastname,filename,file)
                values('$firstname','$lastname','$filename','$name_file')";

        $result=$mydb->query($sql);
        if ($result==1) {
          move_uploaded_file($tmp_name,$local_image.$name_file);
          echo 'uploaded';

          }
        }else {
          $err = true;
        }

  }


?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Submissions Upload </title>



    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>My first Bootstrap 3 demo</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>

  <script>

  </script>
  <body>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
    * {box-sizing: border-box;}

    .errlabel {
      color:red;
    }

    body {
      margin: 0;
      font-family: Arial, Helvetica, sans-serif;
    }


    .topnav {
      overflow: hidden;
      background-color: #e9e9e9;
    }

    .topnav a {
      float: left;
      display: block;
      color: black;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
      font-size: 17px;
    }

    .topnav a:hover {
      background-color: #ddd;
      color: black;
    }

    .topnav a.active {
      background-color: #2196F3;
      color: white;
    }


    .topnav .search-container {
      float: right;
    }

    .topnav input[type=text] {
      padding: 6px;
      margin-top: 8px;
      font-size: 17px;
      border: none;
    }

    .topnav .search-container button {
      float: right;
      padding: 6px;
      margin-top: 8px;
      margin-right: 16px;
      background: #ddd;
      font-size: 17px;
      border: none;
      cursor: pointer;
    }

    .topnav .search-container button:hover {
      background: #ccc;
    }

    @media screen and (max-width: 600px) {
      .topnav .search-container {
        float: none;
      }
      .topnav a, .topnav input[type=text], .topnav .search-container button {
        float: none;
        display: block;
        text-align: left;
        width: 100%;
        margin: 0;
        padding: 14px;
      }
      .topnav input[type=text] {
        border: 1px solid #ccc;
      }
    }

    .cName{
      border-radius: 25px;
      border: 2px solid black;

      width: 300px;
      height: 75px;
      position: relative;
      left: 1400px;
    }

    .email{
      position:absolute;
      TOP:180px;
      LEFT:1750px;
    }

    .link1{
      font-size: 20px;
      font-weight: bold;


    }
    .reason{
      font-size: 20px;
      font-weight: bold;

    }


    .formcopyright{
      margin-left: auto;
      margin-right: auto;

      }

      label{
        color: black;
        font-size: 13pt;
        font-weight: bold;
      }
      .forms {
      /* The image used */

    background-image: url("notes.jpg");
      min-height: 380px;

      /* Center and scale the image nicely */
      background-position: center;
      background-repeat: repeat;
      background-size:200px 200px;
      position: relative;
    }
    form{
      background-color: white;
      padding: 100px;
      margin: auto;
      width: 55%;
      border: 1px black solid  ;
      display: block;
        }




      </style>

    <body>
      <div class="forms" style="text-align:center">
<div class="navbar">


    <div class="topnav">
      <a href="#note" class="navbar-left"><img src="note.jpg" height="25"></a>
      <a href="vhome.php">Home</a>
      <a href="vhome.php#about">About</a>
      <a href="w_feedback.php">Contact</a>
      <a href="vComposerDashboard.php">My Dashboard</a>
      <a href="vcopyrightinfringement.php">Copyright Infringement</a>
      <a href="vMessages.php">Messages</a>
      <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
    </div>

</div>
<form method="post" enctype="multipart/form-data"  action="<?php echo $_SERVER['PHP_SELF']?>"
label  </label>
<label><h2><strong>Submissions Upload</strong></h2> <br> </label><br>
  <label><strong>First Name:</strong><br>
    <input type="text" name="firstname" value="<?php echo $firstname; ?>" />
    <?php
      if ($err && empty($firstname)) {
        echo"<br>";
        echo "<label class='errlabel'>Error: Please enter  first name.</label>";
      }
    ?>
  </label>

  <label><strong>Last Name:</strong> <br>
    <input type="tect" name="lastname" value="<?php echo $lastname; ?>" />
    <?php
      if ($err && empty($lastname)) {
        echo"<br>";
        echo "<label class='errlabel'>Error: Please enter last name.</label>";
      }
    ?>
  </label> <br> <br>


  <label><strong>File Name:</strong> <br>
    <input type="text" name="filename" value="<?php echo $filename; ?>" />
    <?php
      if ($err && empty($filename)) {
        echo"<br>";
        echo "<label class='errlabel'>Error: Please enter the file name.</label>";
      }
    ?>
  </label> <br> <br>

  <label><strong>Upload File:</strong> <br>
    <input type="file" name="file" value="<?php echo $name_file; ?>" />
    <?php
      if ($err && empty($name_file)) {
        echo "<label class='errlabel'>Error: Please upload file.</label>";
      }
    ?>
  </label>
<br>
<br>
<label>
  <input type="submit" name="submit" value="Submit" />
</label>
</form>
<br><br><br> <br><br> <br>
  </div>
</body>
</html>
