<!DOCTYPE html>
<html lang="" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add comment</title>

    <!-- bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- set stylesheet -->
    <link rel="stylesheet" type="text/css" href="tStyles.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- nav bar style/jq -->
    <link rel="stylesheet" href="navbarstyles.css">
    <script type="text/javascript" src="navbarscript.js"></script>

</head>

<body>
    <div class="navbar">
        <div class="topnav">
            <a href="#note" class="navbar-left"><img src="note.jpg" height="25"></a>
            <a class="active" href="#home">Home</a>
            <a href="#about">About</a>
            <a href="#contact">Contact</a>
            <a href="ComposerDashboard.php">My Dashboard</a>
            <div class="search-container">
                <form action="/action_page.php">
                    <input type="text" placeholder="Search.." name="search">
                    <button type="submit">Submit</button>
                </form>
            </div>
        </div>
    </div>

    <h1>Add a comment on song:</h1>
    
    <?php
        $SheetMusicID="";
        $songName = "";
        $songComment = "";
        $err = false;

        if(isset($_GET['songName'])) $songName = $_GET['songName'];
        if(isset($_GET['SheetMusicID'])) $SheetMusicID = $_GET['SheetMusicID'];
        if(isset($_GET["songComment"])) $songComment=$_GET["songComment"];
        
     if (isset($_POST["comment"])) {
        if(isset($_POST["songName"])) $songName=$_POST["songName"];
        if(isset($_POST["SheetMusicID"])) $SheetMusicID=$_POST["SheetMusicID"];
        if(isset($_POST["songComment"])) $songComment=$_POST["songComment"];

        if(!empty($songName) && !empty($songComment)) {
        header("HTTP/1.1 307 Temprary Redirect"); //
        header("Location: jCommentSuccess.php");
        
    } else {
        $err = true;
    }
}
   ?>
   

<form method="post" action='jCommentSuccess.php'>
<p><span class="error">* required field</span></p>
  Song Name: *
  <br>
  <input type="text" name="songName" value="<?php echo $songName;?>">
  <br><br>
  Comment: *
  <br>
  <input type = "text" name="songComment" rows="5" cols="40"></textarea>
  <br><br>
  <input type='hidden' name='SheetMusicID' value='<?php echo $SheetMusicID;?>'>
  <input type="submit" name = "comment" value = "Comment" class="btn btn-primary">
  <br><br><a href="j_view_sheet_music.php"><button type="button" class="btn btn-primary" name="button">Return to view uploaded music page</button></a>


    

</body>

</html>