<!doctype html>
<html>
<head>
  <!-- author - Trentino Longhi -->
  <title>Payment addition success!</title>
</head>
<body>
  <?php
    session_start();
    if(isset($_POST["CardNum"])) $cardNum=$_POST["CardNum"];
    if(isset($_POST["CardCVC"])) $cardCVC=$_POST["CardCVC"];
    if(isset($_POST["CardFirstName"])) $cardFirstName=$_POST["CardFirstName"];
    if(isset($_POST["CardLastName"])) $cardLastName=$_POST["CardLastName"];
    if(isset($_POST["CardAddress"])) $cardAddress=$_POST["CardAddress"];
    if(isset($_POST["PaymentName"])) $paymentName=$_POST["PaymentName"];
    // hardcode - these would be (cookies?)
    $memberID = 101;
    $memberType = 'musician';
  ?>

  <!-- display retrieved form data -->
  <p>
    <strong><?php echo $cardNum; ?></strong> is a card with cvc
    <strong><?php echo $cardCVC; ?></strong>, holder name
    <strong><?php echo "$cardFirstName $cardLastName"; ?></strong>, address
    <strong><?php echo $cardAddress; ?></strong>, and Payment Name
    <strong><?php echo $paymentName; ?></strong>.
    <br>
  </p>

  <?php
    // add a new payment method
    require_once("db.php");
    $sql =
      "INSERT INTO
        dbpaymentmethod(MemberID, MemberType, CardNum, CardCVC, CardFirstName, CardLastName, CardAddress, PaymentName)
      VALUES
        ('$memberID', '$memberType', '$cardNum', '$cardCVC', '$cardFirstName', '$cardLastName', '$cardAddress', '$paymentName')";
    $result=$mydb->query($sql);

    if ($result==1) { // if db table was modified
      echo "result: success! added a payment method for $paymentName.<br><br>";
    } else { // if db table fialed to mod
      echo "result: failure. failed to add a payment method for $paymentName.<br><br>";
    } // end result check
    // echo "$sql <br><br>"; // helps debugging

    // table time!

    //format
    echo "
    <style>
      table, th, td {
        border:1px solid black;
      }
    </style>
    ";

    // table header layout:
    echo "
      <table>
        <thead><tr>
        <th>Card Number</th>
        <th>CVC</th>
        <th>Cardholder Name</th>
        <th>Address</th>
        <th>Payment Method Name</th>
        </tr></thead>
    ";

    // table body
    echo "
        <tr>
         <td>$cardNum</td>
         <td>$cardCVC</td>
         <td>$cardFirstName $cardLastName</td>
         <td>$cardAddress</td>
         <td>$paymentName</td>
        </tr>
      </table>
    ";
   ?>

   <!-- back button -->
  <br><br><br><a href="tManagePayment.php"><button type="button" name="button">return to manage payments page</button></a>

</body>
</html>
