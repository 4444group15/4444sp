<?php
  session_start();
  // $userid="";
  // $userid = $_SESSION["username"];
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Commissioner Dashboard</title>


    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>My first Bootstrap 3 demo</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>


  <body>
    <!DOCTYPE html>
    <html>
    <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
      * {box-sizing: border-box;}

      body {
        margin: 0;
        font-family: Arial, Helvetica, sans-serif;
      }


      .topnav {
        overflow: hidden;
        background-color: #e9e9e9;
      }

      .topnav a {
        float: left;
        display: block;
        color: black;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        font-size: 17px;
      }

      .topnav a:hover {
        background-color: #ddd;
        color: black;
      }

      .topnav a.active {
        background-color: #2196F3;
        color: white;
      }


      .topnav .search-container {
        float: right;
      }

      .topnav input[type=text] {
        padding: 6px;
        margin-top: 8px;
        font-size: 17px;
        border: none;
      }

      .topnav .search-container button {
        float: right;
        padding: 6px;
        margin-top: 8px;
        margin-right: 16px;
        background: #ddd;
        font-size: 17px;
        border: none;
        cursor: pointer;
      }

      .topnav .search-container button:hover {
        background: #ccc;
      }

      @media screen and (max-width: 600px) {
        .topnav .search-container {
          float: none;
        }
        .topnav a, .topnav input[type=text], .topnav .search-container button {
          float: none;
          display: block;
          text-align: left;
          width: 100%;
          margin: 0;
          padding: 14px;
        }
        .topnav input[type=text] {
          border: 1px solid #ccc;
        }
      }

      .cName{
        border-radius: 25px;
        border: 2px solid black;
        padding: 20px;
        width: 300px;
        height: 75px;
        position: relative;
        left: 100px;
      }

      .email{
        position:relative;

        float: right;
      }

      .upsm{
        position: relative;
        left: 100px;
        float: none;
        font-size: 0;
      }
      .vul{
        position: relative;
        left: 150px;
      }
      .ms{
        position: relative;
        left: 200px;
      }
      .mc{
        position: relative;
        left: 250px;
      }

      .cool {
      display:inline-block;
      }
    </style>
  </head>
  <body>
    <div class="navbar">
      <div class="topnav">
        <a href="#note" class="navbar-left"><img src="note.jpg" height="25"></a>
        <a href="vhome.php">Home</a>
        <a href="vhome.php#about">About</a>
        <a href="w_feedback.php">Contact</a>
        <a class="active" href="vCommissionersDashboard.php">My Dashboard</a>
        <a href="vcopyrightinfringement.php">Copyright Infringement</a>
        <a href="vMessages.php">Messages</a>
        <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
      </div>
    </div>

    <br>
    <br>
    <br>
    <br>




    <div style="text-align:center">

      <form class="cool" action="w_Commissioners_Request_Music.php" method="post"><input type="submit"   value=" Request Music " style="height:250px; width: 250px;font-size:15pt;color:white;background-color:gray;border:2px solid;padding:3px"></form>
      <form class="cool" action="w_view_requests_commissioner_View.php" method="post"><input type="submit" value="View Requests " style="height:250px; width: 250px;font-size:15pt;color:white;background-color:gray;border:2px solid;padding:3px"></form>
      <form class="cool" action="tViewSubmissions.php" method="post">  <input type="submit" action ="" value=" View Submissions " style="height:250px; width: 250px;font-size:15pt;color:white;background-color:gray;border:2px solid;padding:3px"></form>
      <form class="cool" action="tViewOwnedMusic.php" method="post">  <input type="submit" action ="" value=" Music Owned " style="height:250px; width: 250px;font-size:15pt;color:white;background-color:gray;border:2px solid;padding:3px"></form>
      <form class="cool" action="tManagePayment.php" method="post"><input type="submit" value=" Payment Methods " style="height:250px; width: 250px;font-size:15pt;color:white;background-color:gray;border:2px solid;padding:3px"></form>
      <form class="cool" action="tManageReception.php" method="post"><input type="submit" value=" Reception Methods " style="height:250px; width: 250px;font-size:15pt;color:white;background-color:gray;border:2px solid;padding:3px"></form>

    </div>


</body>
</html>
