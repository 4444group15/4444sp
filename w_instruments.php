<?php
session_start();
$item1="";
$item2="";

if(isset($_POST['submit'])){
  $item1=$_POST['item1'];
  $item2=$_POST['item2'];
}
if(isset($_POST['clear'])){
  $_POST['item1']="";
  $_POST['item2']="";
}


 ?>
 <head>
   <title>Instrument Pairings</title>
   <!-- bootstrap -->
   <link href="css/bootstrap.min.css" rel="stylesheet" />
   <script src="jquery-3.1.1.min.js"></script>
   <script src="js/bootstrap.min.js"></script>

   <!-- set stylesheet -->
   <link rel="stylesheet" type="text/css" href="tStyle.css">

   <!-- nav bar style/jq -->
   <link rel="stylesheet" href="navbarstyles.css">
   <script type="text/javascript" src="navbarscript.js"></script>

   <script type="text/javascript">
   function clearform()
   {
    document.getElementById("item1").value=""; //don't forget to set the textbox id
    document.getElementById("item2").value="";
  }

   </script>



 </head>
 <style media="screen">
   .floating{
     float: left;
     padding-bottom: 50px;
   }
   .floatingButtons{
     float: left;
   }
 </style>

 <body>
   <div class="topnav">
     <a class="navbar-left"><img src="note.jpg" height="25"></a>
     <a class="active" href="vhome.php">Home</a>
     <a href="vhome.php#about">About</a>
     <a href="w_feedback.php">Contact</a>
     <?php
     $type="";
     $type=$_SESSION['acctype'];
     $goto='';
     //if($type="Musician") $goto='vMusicDashboard.php';
     if($type="Composer") $goto='vComposerDashboard.php';
     if($type="Commissioner") $goto='vCommissionersDashboard.php'; ?>
     <a href="<?php echo $goto; ?>">My Dashboard</a>
     <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>
   </div>
 </div>
<div class="floating">
 <h1 style="margin-left:20px">Which instruments pair well together?</h1>
 <p style="margin-left:20px">Enter in some of the instruments you see in the array below,
   such as "Tuba" and "cello" or "horn" and "voice". <br/> The results
   will tell you whether these instruments are typically
   found together in a request!</p>

     <form class="" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
       <label style="margin-left:20px">Instrument 1:</label>
       <input style="margin-left:20px" type="text" id="item1" name="item1" value="<?php echo $item1; ?>">
       <br/>
       <label style="margin-left:20px">Instrument 2:</label>
       <input style="margin-left:20px" type="text" id="item2" name="item2" value="<?php echo $item2; ?>">
       <br/>
       <br/>
       <input class="floatingButtons" style="margin-left:20px" type="submit" name="submit" value="submit">
       <input class="floatingButtons"  type="submit" name="clear" value="clear">
     </form>


</div>

   </body>

   <?php

     require __DIR__ . '/vendor/autoload.php';

     //Apriori Associator
     use Phpml\Association\Apriori;

     //Accuracy
     use Phpml\Metric\Accuracy;
     $actuallabels = [];

     $associator = new Apriori($support = 0.3, $confidence = 0.3);

     //make new data from request contents
     //goal is to figure out which instruments go well together
     require_once("db.php");
     $sql='select * from dbrequest';
     $result = $mydb->query($sql);
     $oneArray = Array();
     //$twoArray = Array();
     while ($row = mysqli_fetch_array($result)) {
        $oneArray[] =  $row['requestContent'];

        //$addOn=explode(" ", $row['requestContent']);
        //echo explode(" ", $row['requestContent']);
        //$twoArray->append(array($addOn));

      }
      $newSamples=Array();
      for ($i = 0; $i < count($oneArray); $i++) {
        $arrayValue=$oneArray[$i];
        $new=explode(" ", $arrayValue);
        array_push($newSamples,$new);
        //echo "<br/>".print_r($new)."<br/>";

      }
      ///echo implode("<br/>", $newSamples);


      //echo implode("</br>", $newSamples);
        //echo implode("</br>", $oneArray);

     //training
     $samples = $newSamples;
     //[['Golf Balls', 'Gloves', 'Advil'], ['Golf Balls', 'Running Shoes', 'Socks'], ['Advil', 'Weights', 'Gloves'], ['Golf Balls', 'Advil'], ['Driver', 'Golf Shoes' , 'Basketball'],['Jogging Stroller', 'Running Shoes', 'Ben Gay']];
     $labels  = [];

     $associator->train($samples, $labels);  //train the association rules

     //get rules
     $rules = $associator->getRules(); //get association rules after training

     //print out all the rules
     // for($i=0;$i<sizeof($rules);$i++){
     //   for($j=0;$j<sizeof($rules[$i]["antecedent"]);$j++) {
     //     if($j>0) echo ", ";
         //echo $rules[$i]["antecedent"][$j];  //get the antecedent items
       //}
     //   echo "=>";
     //   echo $rules[$i]["consequent"][0];  //get the consequent item
     //   echo " (s=".$rules[$i]["support"]; //get the support level
     //   echo ", c=".$rules[$i]["confidence"].")<br />"; //get the confidence level
     // }
     // echo "<br />";

     //Predict
     $result = $associator->predict([$item1]);
     //echo $result[0][0]."<br />";

     //$result = $associator->predict([[$_SESSION['item1'],$_SESSION['item2']]]); //returns an array
     $together="";


     for($i=0;$i<sizeof($result);$i++){
       if($item2==$result[$i][0]){
         $together=true;
       }
     }

      if($together){
        echo "<br/><div class='floating'><textarea cols='65' rows='89' style='margin-left:20px;background-color:steelblue;color:white;'>These two instruments sound great together!
        ";
        echo print_r($newSamples)."</textarea></div>";
        echo "<div style='padding-bottom:50px'/>";
      }
      if($together==false){
        echo "<br/><div class='floating'><textarea  cols='65' rows='89' style='margin-left:20px;background-color:gray;color:white;'>These two instruments are typically NOT placed together.
        ";
        echo print_r($newSamples)."</textarea></div>";
      }




    ?>
 </html>
