<?php
  session_start();
  $type=$_SESSION['acctype'];
  $productID = 0;
  $productName = "";
  $supplierID = "";
  $message = "";
  $firstname = "";
  $lastname = "";
  $fullname = "";
  //$fname="";
//$lname="";
//session_start();
//$fname=$_SESSION["firstName"];
///$lname=$_SESSION["lastName"];
//$myName=$fname." ".$lname;




  $err = false;

  if (isset($_POST["submit"])) {
    if(isset($_POST["fname"])) $productName = $_POST["fname"];
    if(isset($_POST["lname"])) $supplierID = $_POST["lname"];
    if(isset($_POST["fullname"])) $fullname = $_POST["fullname"];
    if(isset($_POST["name"])) $message = $_POST["name"];


    if (!empty($productName) && !empty($supplierID) && !empty($message)) {
      require_once("db.php");

      if ($productID == 0) {
        $sql = "insert into dbmessages(FirstName, LastName, SendersName, Message)
                values('$productName', '$supplierID','$fullname', '$message')";
        $result=$mydb->query($sql);
        if ($result==1) {
          echo "<script>alert('Message Sent');</script>";

        }

      }

    } else {
      $err = true;
    }
  }




?>


<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Message  </title>
    <meta name="viewport content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </head>

  <body>

    <style>
    * {box-sizing: border-box;}

    body {
      margin: 0;
      font-family: Arial, Helvetica, sans-serif;
    }


    .topnav {
      overflow: hidden;
      background-color: #e9e9e9;
    }

    .topnav a {
      float: left;
      display: block;
      color: black;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
      font-size: 17px;
    }

    .topnav a:hover {
      background-color: #ddd;
      color: black;
    }

    .topnav a.active {
      background-color: #2196F3;
      color: white;
    }


    .topnav .search-container {
      float: right;
    }

    .topnav input[type=text] {
      padding: 6px;
      margin-top: 8px;
      font-size: 17px;
      border: none;
    }

    .topnav .search-container button {
      float: right;
      padding: 6px;
      margin-top: 8px;
      margin-right: 16px;
      background: #ddd;
      font-size: 17px;
      border: none;
      cursor: pointer;
    }

    .topnav .search-container button:hover {
      background: #ccc;
    }

    @media screen and (max-width: 600px) {
      .topnav .search-container {
        float: none;
      }
      .topnav a, .topnav input[type=text], .topnav .search-container button {
        float: none;
        display: block;
        text-align: left;
        width: 100%;
        margin: 0;
        padding: 14px;
      }
      .topnav input[type=text] {
        border: 1px solid #ccc;
      }
    }


    /* Style the tab */
  .tab {
    overflow: hidden;
    border: 1px solid #ccc;
    border-color: white;
    background-color: white;
  }

  /* Style the buttons that are used to open the tab content */
  .tab button {
    background-color: #f1f1f1;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
  }

  /* Change background color of buttons on hover */
  .tab button:hover {
    background-color: #ddd;
  }

  /* Create an active/current tablink class */
  .tab button.active {
    background-color: #ccc;
  }

  /* Style the tab content */
  .tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
  }

  body{
    background-image: url("emails.jpg");
      min-height: 380px;

      /* Center and scale the image nicely */
      background-position: center;
      background-repeat: none;
      background-size:300px 300px;
      position: relative;
  }
  .tabcontent{
    background-color: white;
  }

  table, td {
    border: 1px solid white;
  }
  table {
    border-collapse: collapse;
    empty-cells: show;
    display:
  }
  th, td:first-child{
    color: white;
    background-color: gray;
    width: 40%;
  }
  td {
    width: 15em;
    height: 20px;
    color: black;
    background-color: #ddd;
  }
      .errlabel{
        color: red;
      }

      </style>

    <body>
<div class="navbar">


    <div class="topnav">
      <a href="#note" class="navbar-left"><img src="note.jpg" height="25"></a>
      <a href="vhome.php">Home</a>
      <a href="vhome.php#about">About</a>
      <a href="w_feedback.php">Contact</a>
      <?php
      $goto='';
      if($type=="Musician") $goto='vMusicDashboard.php';
      if($type=="Composer") $goto='vComposerDashboard.php';
      if($type=="Commissioner") $goto='vCommissionersDashboard.php'; ?>
      <a href="<?php echo $goto; ?>">My Dashboard</a>
      <a style="text-align:right;float:right;" href="ulogout.php">Logout</a>

    </div>

</div>


<!-- Tab links -->
<div class="tab">
  <button id= "mybutton" class="tablinks" onclick="openContent(event, 'SendMessage')">Send Message</button>
  <script>
    document.addEventListener("DOMContentLoaded", function(event) {
      document.getElementById("mybutton").click();
   });
  </script>
  <button class="tablinks" onclick="openContent(event, 'ViewMessage')">View Message</button>
</div>

<div id="SendMessage" class="tabcontent">

  Please enter the first and last name of the person the message is being sent to.
  <form class="sendmessage" action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
    <label>
      First Name: <input type="text" name="fname" value="<?php echo $productName; ?>">
      <?php
        if ($err && empty($firstname)) {
            echo "<br>";
          echo "<label class='errlabel'>Error: Please enter your first name.</label>";
        }
      ?>
    </label>
    <label>
      Last Name: <input type="text" name="lname" value="<?php echo $supplierID; ?>">
      <?php
        if ($err && empty($lastname)) {
          echo "<br>";
          echo "<label class='errlabel'>Error: Please enter your last name.</label>";
        }
      ?>
    </label> <br>
    Please enter your first and last name. <br>
    <label>
    Your Name: <input type="text" name="fullname" value="<?php echo $fullname; ?>">
      <?php
        if ($err && empty($fullname)) {
            echo "<br>";
          echo "<label class='errlabel'>Error: Please enter the your full name.</label>";
        }
      ?>
    </label> <br>
    <label>
      Message:<br>
      <textarea name="name" value = "<?php echo $message; ?>" rows="8" cols="80"></textarea>
      <?php
        if ($err && empty($message)) {
            echo "<br>";
          echo "<label class='errlabel'>Error: Please enter your message.</label>";
        }
      ?>
    </label><br>
    <input type="submit" name="submit" value="Submit">
  </form>
</div>

<div id="ViewMessage" class="tabcontent">
  <form class="sendmessage" action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
    <label>
      First Name: <input type="text" name="firstname" value="<?php echo $firstname; ?>">
      <?php
        if ($err && empty($firstname)) {
          echo "<br>";
          echo "<label class='errlabel'>Error: Please enter the reference link.</label>";
        }
      ?>
    </label>
    <label>
      Last Name: <input type="text" name="lastname" value="<?php echo $lastname; ?>">
      <?php
        if ($err && empty($lastname)) {
          echo "<br>";
          echo "<label class='errlabel'>Error: Please enter a reason.</label>";
        }
      ?>
    </label>
      <input type="submit" name="sub" value="Submit">
    </form>

    <?php
    if (isset($_POST["sub"])) {
      if(isset($_POST["firstname"])) $firstname = $_POST["firstname"];
      if(isset($_POST["lastname"])) $lastname = $_POST["lastname"];

      if (!empty($firstname) && !empty($lastname)) {

        require_once("db.php");

        $sql = "select id, FirstName, LastName, Message, SendersName
                from dbmessages
                where FirstName = '$firstname' and LastName = '$lastname'";
        $result = $mydb->query($sql);
        if ($result== true) {
        echo "<table>";
        echo "<thead>";
        echo "<th>Senders Name</th>  <th>Message</th> <th>Delete Message</th>";

        //echo "<th>Senders Name</th>  <th>Message</th>  <th> Delete </th>";
        echo "</thead>";

          while($row = mysqli_fetch_array($result)) {

            echo "<tr>";
            echo "<td>".$row["SendersName"]."</td>";
            echo "<td>".$row["Message"]."</td>";

          echo "<td>"."<input type='submit' value='delete'  onclick='SomeDeleteRowFunction(this)' name='".$row["id"]."'>"."</td>";

            echo "</tr>";

          }
        }
      }else {
        $err = true;
      }

    }
     ?>
       <div id="contentArea">&nbsp;</div>
</div>


<script>
function openContent(evt, content) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(content).style.display = "block";
  evt.currentTarget.className += " active";
}


function SomeDeleteRowFunction(e) {
     // event.target will be the input element.
     var td = event.target.parentNode;
     var tr = td.parentNode; // the row to be removed
     var id=e.name;
     console.log(id);

     $.ajax({
       url:'vdeletemessagesbyid.php?id='+id,
       async:true,
       success: function(result){
          $("#contentArea").html(result);
      }
    });
     //ajax
     //deleteMessageByid.php+id
     tr.parentNode.removeChild(tr);
}
</script>

</body>
</html>
